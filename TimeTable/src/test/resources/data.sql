INSERT INTO users (status_id, email, class_mark, password, firstname, secondname, student_role, dossier)
VALUES
	(1, 'email', 'student', '$2a$10$trT3.R/Nfey62eczbKEnueTcIbJXW.u1ffAo/XfyLpofwNDbEB86O', 'firstname', 'secondname', 'REGULAR', 'dossier'),
	(1, 'another email', 'teacher', '$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2', 'another firstname', 'another secondname', 'LIBRARIAN', 'dossier');

INSERT INTO groups (name)
VALUES
	('name'),
	('another name');

INSERT INTO in_campus_addresses	(building, entry, floor)
VALUES
	('building', 1, 1);
	
INSERT INTO auditories (address_id, name)
VALUES 
	(1, 'name');
	
INSERT INTO courses (name, description)
VALUES
	('name', 'description');
	
INSERT INTO groups_to_sutdents_assigments (student_id, group_id)
VALUES
	(1, 1);
	
INSERT INTO lessons (teacher_id, auditory_id, course_id, start_date, end_date)
VALUES
	(2, 1, 1, '2000-05-05 01:01:01', '2000-05-05 01:01:01');
	
INSERT INTO lessons (teacher_id, auditory_id, course_id, start_date, end_date)
VALUES
	(2, 1, 1, '2000-05-06 01:01:01', '2000-05-06 01:01:01');
	
INSERT INTO lessons_to_groups_assigments (lesson_id, group_id)
VALUES
	(1, 1);	