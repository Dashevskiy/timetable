package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.entity.Group;

@ExtendWith(MockitoExtension.class)
class GroupUpdateRequestVallidatorImplTest {
    
    @Mock
    private GroupCrud groupCrud;
    
    @InjectMocks    
    private GroupUpdateRequestVallidatorImpl updateGroupRequestVallidator;
    
    @Test
    void validateShouldInvokeAllValidationMethodsWithRightArguments() {
        GroupUpdateRequest groupupdateRequest = new GroupUpdateRequest(1 ,false, "name");
        updateGroupRequestVallidator = spy(updateGroupRequestVallidator);
        doNothing().when(updateGroupRequestVallidator).validateNotNull("name", "Name is null.");
        doNothing().when(updateGroupRequestVallidator).validateNotEmpty("name", "Name is empty.");
        doNothing().when(updateGroupRequestVallidator).validateNameIsUnique("name"); 
        updateGroupRequestVallidator.validate(groupupdateRequest);
        
        verify(updateGroupRequestVallidator).validateNotNull("name", "Name is null.");
        verify(updateGroupRequestVallidator).validateNotEmpty("name", "Name is empty.");
        verify(updateGroupRequestVallidator).validateNameIsUnique("name");        
    }
    
    @Test
    void validateNameIsUniqueShouldThrowAnExeptionIfCrudReturnEmptyOptional() {
        when(groupCrud.findByName("name")).thenReturn(Optional.empty());
        
        assertThrows(IllegalArgumentException.class, 
                     () -> updateGroupRequestVallidator.validateNameIsUnique("name"),
                     "Name is not unique.");
    }
    
    @Test
    void validateNameIsUniqueShouldNotThrowAnExeptionIfCrudReturnNotEmptyOptional() {
        when(groupCrud.findByName("name")).thenReturn(Optional.of(new Group(anyInt(), false, "name")));
        
        assertDoesNotThrow(() -> updateGroupRequestVallidator.validateNameIsUnique("name"));
    }
}
