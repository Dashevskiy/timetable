package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import ru.com.dashevskiy.university.validator.Validator;

class DefaultsTestValidatorImplTest {
    private final Validator<Object> validator = new DefaultsTestValidatorImpl();
    
    @Test
    void validateNotNullShuoldThrowExeptionWtihGivenMessageWhenPassNullValue() {
        assertThrows(IllegalArgumentException.class, 
                () -> validator.validateNotNull(null, "exeptionMessage"), "exeptionMessage");
    }
    
    @Test
    void validateNotNullShouldNotThrowExeptionWhenGetsNotNullObject() {
        assertDoesNotThrow(() -> validator.validateNotNull("some string", "exeptionMessage")); 
    }
    
    @Test
    void validateNotEmptyShuoldThrowExeptionWtihGivenMessageWhenPassEmptyString() {
        assertThrows(IllegalArgumentException.class, 
                () -> validator.validateNotEmpty("", "exeptionMessage"), "exeptionMessage");
    }
    
    @Test
    void validateNotEmptyShouldNotThrowExeptionWhenGetsNotEmptyString() {
        assertDoesNotThrow(() -> validator.validateNotEmpty("some string", "exeptionMessage")); 
    }

}
