package ru.com.dashevskiy.university.dao.crudimpl;

import org.junit.jupiter.api.Test;

import ru.com.dashevskiy.university.entity.Auditory;
import ru.com.dashevskiy.university.entity.InCampusAdress;

class AuditoryCrudImplTest extends CrudDaoTest<Auditory> {    
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {        
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(auditoryCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {        
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(auditoryCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {        
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(auditoryCrud);
    }
        
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(auditoryCrud);
    }
    
    @Test    
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(auditoryCrud);
    }
    
    @Test    
    void updateShouldUpdateObjectInDatabaseWhenPassValidGroup() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(auditoryCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(auditoryCrud);
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(auditoryCrud);
    }
    
    @Override
    protected Auditory getTestObject(int id) {
        inCampusAdressCrud.save(InCampusAdress.builder()
                .withId(1)
                .build());
        return new Auditory(id, 1, "name");
    }

    @Override
    protected Auditory getAnotherTestObject(int id) {
        inCampusAdressCrud.save(InCampusAdress.builder()
                .withId(2)
                .build());
        return new Auditory(id, 2, "another name");
    }
}
