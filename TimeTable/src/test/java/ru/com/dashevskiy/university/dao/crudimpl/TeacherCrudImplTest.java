package ru.com.dashevskiy.university.dao.crudimpl;

import org.junit.jupiter.api.Test;

import ru.com.dashevskiy.university.entity.user.Teacher;

class TeacherCrudImplTest extends CrudDaoTest<Teacher> {
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(teacherCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {        
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(teacherCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(teacherCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(teacherCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(teacherCrud);
    }
    
    @Test
    void updateShouldUpdateObjectInDatabaseWhenPassValidObject() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(teacherCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(teacherCrud);
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(teacherCrud);
    }
    
    @Override
    protected Teacher getTestObject(int id) {
        return Teacher.builder()
                .withId(id)
                .withEmail("email")
                .withPassword("$2a$10$trT3.R/Nfey62eczbKEnueTcIbJXW.u1ffAo/XfyLpofwNDbEB86O")
                .withFirstname("firstname")
                .withSecondname("secondname")
                .withDossier("dossier")
                .build();
    } 

    @Override
    protected Teacher getAnotherTestObject(int id) {
        return Teacher.builder()
                .withId(id)
                .withEmail("another email")
                .withPassword("$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2")
                .withFirstname("another firstname")
                .withSecondname("another secondname")
                .withDossier("another dossier")
                .build();
    }
}
