package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;
import ru.com.dashevskiy.university.entity.Group;
import ru.com.dashevskiy.university.entity.user.Student;

@ExtendWith(MockitoExtension.class)
public class GroupStudenAssignationRequestValidatorImplTest {
    
    @Mock
    private StudentCrud studentCrud;
    
    @Mock
    private GroupCrud groupCrud;
    
    @InjectMocks
    private GroupStudenAssignationRequestValidatorImpl groupStudenAssignationRequestValidator;
    
    @Test
    void validateShouldInvokeAllValidationMethodsWithRightArguments() {
        Set<Integer> studentIds = new HashSet<>();
        studentIds.add(1);
        studentIds.add(2);
        GroupStudenAssignationRequest groupStudenAssignationRequest = new GroupStudenAssignationRequest(studentIds, 1);        
        
        groupStudenAssignationRequestValidator = spy(groupStudenAssignationRequestValidator);
        doNothing().when(groupStudenAssignationRequestValidator).validateGroupExists(1);
        doNothing().when(groupStudenAssignationRequestValidator).validateStudentsExist(studentIds);
        doNothing().when(groupStudenAssignationRequestValidator)
                .validateNoSuchAssignationExistAlready(groupStudenAssignationRequest);
        groupStudenAssignationRequestValidator.validate(groupStudenAssignationRequest);
        
        verify(groupStudenAssignationRequestValidator).validateGroupExists(1);
        verify(groupStudenAssignationRequestValidator).validateStudentsExist(studentIds);
        verify(groupStudenAssignationRequestValidator)
                .validateNoSuchAssignationExistAlready(groupStudenAssignationRequest);        
    }
    
    @Test
    void validateNoSuchAssignationExistAlreadyShuldThrowExeptionWithConflictingStudentsMessageWhenConflictingStudentsPassed() {
        List<Student> studentsFromDao = new ArrayList<>();
        studentsFromDao.add(Student.builder().withId(1).build());
        studentsFromDao.add(Student.builder().withId(2).build());
        studentsFromDao.add(Student.builder().withId(3).build());
        studentsFromDao.add(Student.builder().withId(4).build());
        
        Set<Integer> studentsForAssignationIds = new HashSet<>();
        studentsForAssignationIds.add(2);
        studentsForAssignationIds.add(3);
        
        when(studentCrud.findStudentsByGroupId(1)).thenReturn(studentsFromDao);
        
        assertThrows(IllegalArgumentException.class, 
                     () -> groupStudenAssignationRequestValidator.validateNoSuchAssignationExistAlready(
                             new GroupStudenAssignationRequest(studentsForAssignationIds, 1)),
                     "Students with ids 2, 3 have already been assigned to group with - 1");
    }
    
    @Test
    void validateGroupExistsShouldThrowAnExeptionIfPassedNotExistingGroupId() {
        when(groupCrud.findById(1)).thenReturn(Optional.empty());
        
        assertThrows(NoSuchElementException.class, 
                     () -> groupStudenAssignationRequestValidator.validateGroupExists(1),
                     "Group with id 1 is not existing.");
    }
    
    @Test
    void validateGroupExistsShouldNotThrowAnExeptionIfPassedExistingGroupId() {
        when(groupCrud.findById(1)).thenReturn(Optional.of(new Group(1, false, "name")));        
        
        assertDoesNotThrow(() -> groupStudenAssignationRequestValidator.validateGroupExists(1));       
    }
    
    @Test
    void validateNoSuchAssignationExistAlreadyShuldNotThrowExeptionWhenNotConflictingStudentsPassed() {
        List<Student> studentsFromDao = new ArrayList<>();
        studentsFromDao.add(Student.builder().withId(1).build());        
        studentsFromDao.add(Student.builder().withId(4).build());
        
        Set<Integer> studentsForAssignationIds = new HashSet<>();
        studentsForAssignationIds.add(2);
        studentsForAssignationIds.add(3);
        
        when(studentCrud.findStudentsByGroupId(1)).thenReturn(studentsFromDao);
        
        assertDoesNotThrow(() -> groupStudenAssignationRequestValidator.validateNoSuchAssignationExistAlready(
                             new GroupStudenAssignationRequest(studentsForAssignationIds, 1)));        
    }
        
    @Test
    void validateStudentsExistShouldThrowExeptionWithExistingStudentsMessageWhenNotExistingStudentsPassed() {               
        Set<Integer> studentsForAssignationIds = new HashSet<>();
        studentsForAssignationIds.add(1);
        studentsForAssignationIds.add(2);
        studentsForAssignationIds.add(3);
        
        when(studentCrud.findById(1)).thenReturn(Optional.of(Student.builder().withId(1).build()));
        when(studentCrud.findById(2)).thenReturn(Optional.empty());
        when(studentCrud.findById(3)).thenReturn(Optional.empty());
        
        assertThrows(NoSuchElementException.class, 
                     () -> groupStudenAssignationRequestValidator.validateStudentsExist(studentsForAssignationIds),
                     "Students with ids 2, 3 are not existing.");
    }
    
    @Test
    void validateStudentsExistShouldNotThrowExeptionIfExistingStudentsPassed() {               
        Set<Integer> studentsForAssignationIds = new HashSet<>();
        studentsForAssignationIds.add(1);
        studentsForAssignationIds.add(2);
        studentsForAssignationIds.add(3);
        
        when(studentCrud.findById(1)).thenReturn(Optional.of(Student.builder().withId(1).build()));
        when(studentCrud.findById(2)).thenReturn(Optional.of(Student.builder().withId(2).build()));
        when(studentCrud.findById(3)).thenReturn(Optional.of(Student.builder().withId(3).build()));
        
        assertDoesNotThrow(
                () -> groupStudenAssignationRequestValidator.validateStudentsExist(studentsForAssignationIds));        
    }
}
