package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.entity.Group;

@ExtendWith(MockitoExtension.class)
class GroupAddRequestVallidatorImplTest {
    
    @Mock
    private GroupCrud groupCrud;
    
    @InjectMocks    
    private GroupAddRequestVallidatorImpl addGroupRequestVallidator;
    
    @Test
    void validateShouldInvokeAllValidationMethodsWithRightArguments() {
        GroupAddRequest groupAddRequest = new GroupAddRequest("name");
        addGroupRequestVallidator = spy(addGroupRequestVallidator);
        doNothing().when(addGroupRequestVallidator).validateNotNull("name", "Name is null.");
        doNothing().when(addGroupRequestVallidator).validateNotEmpty("name", "Name is empty.");
        doNothing().when(addGroupRequestVallidator).validateNameIsUnique("name"); 
        addGroupRequestVallidator.validate(groupAddRequest);
        
        verify(addGroupRequestVallidator).validateNotNull("name", "Name is null.");
        verify(addGroupRequestVallidator).validateNotEmpty("name", "Name is empty.");
        verify(addGroupRequestVallidator).validateNameIsUnique("name");        
    }
    
    @Test
    void validateNameIsUniqueShouldThrowAnExeptionIfCrudReturnEmptyOptional() {
        when(groupCrud.findByName("name")).thenReturn(Optional.empty());
        
        assertThrows(IllegalArgumentException.class, 
                     () -> addGroupRequestVallidator.validateNameIsUnique("name"),
                     "Name is not unique.");
    }
    
    @Test
    void validateNameIsUniqueShouldNotThrowAnExeptionIfCrudReturnNotEmptyOptional() {
        when(groupCrud.findByName("name")).thenReturn(Optional.of(new Group(anyInt(), false, "name")));
        
        assertDoesNotThrow(() -> addGroupRequestVallidator.validateNameIsUnique("name"));
    }
}
