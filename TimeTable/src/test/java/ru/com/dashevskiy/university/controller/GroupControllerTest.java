package ru.com.dashevskiy.university.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import ru.com.dashevskiy.university.service.GroupService;

class GroupControllerTest extends AbstractControllerTest{
    
    @MockBean
    private GroupService groupService;
    
    @Autowired
    private GroupController controller;
    
    @Value("${groups.controller.itemsOnPage}")
    private Integer itemPerPage;
    
    @Value("${groups.controller.defaultPage}")
    private Integer defaultPage;
    
    @Override
    @BeforeEach
    public void initalize() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setSuffix(".html");
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setViewResolvers(viewResolver)
                .build();
    }
    
    @Test
    void performingGroupsCallShouldCallgroupsTemplateWithOkStatus() throws Exception {
        mockMvc.perform(get("/groups").param("page", "0"))
            .andExpect(status().isOk())
            .andExpect(view().name("groups"));
    }
    
    @Test
    void groupsCallModelShuldConntainParameterWithPassedPageParrameter() throws Exception {
        when(groupService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/groups").param("page", "5"))
            .andExpect(model().attribute("page", equalTo(5)));
    }
    
    @Test
    void performingGroupsCallShouldCallFindAllMethodFromStudetnServiceWithAppropriateArguments() throws Exception {
        when(groupService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/groups").param("page", "5"));
        
        verify(groupService).findAll(4, itemPerPage);
    }
    
    @Test
    void performingGroupsCallShouldReturnDefaultPageWhenCalledWithoutParameter() throws Exception {
        when(groupService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/groups"));
        
        verify(groupService).findAll(defaultPage - 1, itemPerPage);
    }
    
    @Test
    void performingGroupsCallShouldReturnDefaultPageWhenCalledWithPageLessThanOne() throws Exception {
        when(groupService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/groups").param("page", "0"));
        
        verify(groupService).findAll(defaultPage - 1, itemPerPage);
    }
    
    @Test
    void performingGroupsCallShouldReturnMaximalPageWhenCalledWithPageBiggerThanTotalPages() throws Exception {
        when(groupService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/groups").param("page", "12"));
        
        verify(groupService).findAll(10, itemPerPage);
    }
}
