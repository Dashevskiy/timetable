package ru.com.dashevskiy.university.service.implementation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.domain.student.StudentResponse;
import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;
import ru.com.dashevskiy.university.entity.user.Student;
import ru.com.dashevskiy.university.service.impl.StudentServiceImpl;
import ru.com.dashevskiy.university.service.mapper.StudentMapper;
import ru.com.dashevskiy.university.service.mapper.StudentRegisterRequestMapper;
import ru.com.dashevskiy.university.service.mapper.StudentUpdatePersonalDataRequestMapper;
import ru.com.dashevskiy.university.validator.Validator;

@ExtendWith(MockitoExtension.class)
public class StudentServiceImplTest {
    
    @Mock 
    PasswordEncoder passwordEncoder;
    
    @Mock
    StudentCrud studentCrud;
    
    @Mock
    Validator<StudentRegisterRequest> registerRequestValidator;
    
    @Mock
    Validator<StudentUpdatePersonalDataRequest> updatePersonalDataRequestValidator;
    
    @Mock
    StudentRegisterRequestMapper registerRequestMapper;
    
    @Mock
    StudentUpdatePersonalDataRequestMapper updatePersonalDataRequestMapper;
    
    @Mock
    StudentMapper studentMapper;
    
    @Mock
    StudentServiceImpl studentService; 
    
    @BeforeEach
    private void createMocks() {
        studentService = new StudentServiceImpl(passwordEncoder,
                                                studentCrud,
                                                updatePersonalDataRequestValidator,
                                                registerRequestValidator,
                                                registerRequestMapper,
                                                updatePersonalDataRequestMapper,
                                                studentMapper);
    }
    
    @Test
    void registerShouldCallMethodsInAppropriateOrderWhenInvoked() {
        InOrder inOrder = inOrder(registerRequestValidator, passwordEncoder, registerRequestMapper, studentCrud);
        studentService.register(StudentRegisterRequest.builder().build());
        
        inOrder.verify(registerRequestValidator).validate(any());
        inOrder.verify(passwordEncoder).encode(any());
        inOrder.verify(registerRequestMapper).mapDomainToEntity(any(), any());
        inOrder.verify(studentCrud).save(any());                     
    }
    
    @Test
    void findShouldReturnStudentResponseWithGivenIdIfHeIsExistingInTheDatabase() {
        Student student = Student.builder().withId(1).build();
        StudentResponse studentResponse = StudentResponse.builder().withId(1).build();
        
        when(studentCrud.findById(1)).thenReturn(Optional.of(student));
        when(studentMapper.mapEntityToDomain(student)).thenReturn(studentResponse);
        
        assertThat(studentService.find(1), equalTo(studentResponse));
    }
    
    @Test
    void findShouldThrowExeptionIfThereIsNoExistingStudentWithGivenIdInTheDatabase() {
        when(studentCrud.findById(1)).thenReturn(Optional.empty());        
        
        assertThrows(NoSuchElementException.class, 
                     () -> studentService.find(1), 
                     "Did not find student with such id - 1");
    }
    
    @Test
    void findAllShouldInvokeDaoFindAllMethodAndMapperWhenInvoked() {
        final ArrayList<Student> students = new ArrayList<Student>();
        students.add(Student.builder().withId(1).build());
        
        when(studentCrud.findAll(1, 1)).thenReturn(students);
        InOrder inOrder = inOrder(studentCrud, studentMapper);
        studentService.findAll(1, 1);
        
        inOrder.verify(studentCrud).findAll(1, 1);
        inOrder.verify(studentMapper).mapEntityToDomain(any());
    }
    
    @Test
    void findStudentsByGroupIdShoulInwokeDaofindStudentsByGroupIdMethodWithGivenId() {
        final ArrayList<Student> students = new ArrayList<Student>();
        students.add(Student.builder().withId(1).build());
        
        when(studentCrud.findStudentsByGroupId(1)).thenReturn(students);
        InOrder inOrder = inOrder(studentCrud, studentMapper);
        studentService.findStudentsByGroup(1);
        
        inOrder.verify(studentCrud).findStudentsByGroupId(1);
        inOrder.verify(studentMapper).mapEntityToDomain(any());
    }
    
    @Test
    void updatePersonalDataShuldInvokeMethodsInAppropriateOrder() {
        
        when(studentCrud.findById(1)).thenReturn(Optional.of(Student.builder().build()));
        
        InOrder inOrder = inOrder(updatePersonalDataRequestValidator, 
                                  studentCrud, 
                                  updatePersonalDataRequestMapper, 
                                  studentCrud);
        
        studentService.updatePersonalData(StudentUpdatePersonalDataRequest.builder().withId(1).build());
        
        inOrder.verify(updatePersonalDataRequestValidator).validate(any());
        inOrder.verify(studentCrud).findById(1);
        inOrder.verify(updatePersonalDataRequestMapper).mapDomainToEntity(any(), any());
        inOrder.verify(studentCrud).update(any());
    }
   
    @Test
    void loginShouldThrowAnExeptionWhenPassEmailOfNonExistingStudent () {
        when(studentCrud.findStudentByEmail("email")).thenReturn(Optional.empty());
        
        assertThrows(NoSuchElementException.class, 
                     () -> studentService.login("email", "password"),
                     "Did not find student with such email - email");
    }
    
    @Test
    void loginShouldReturnFalseIfPasswordDoesNotMatch () {
        when(studentCrud.findStudentByEmail("email")).thenReturn(Optional.of(Student.builder().build()));
        when(studentService.login("email", "password")).thenReturn(false);
        
        assertThat(studentService.login("email", "password"), equalTo(false));
    }
    
    @Test
    void loginShouldReturnTrueIfPasswordMatches () {
        when(studentCrud.findStudentByEmail("email")).thenReturn(Optional.of(Student.builder().build()));
        when(studentService.login("email", "password")).thenReturn(true);
        
        assertThat(studentService.login("email", "password"), equalTo(true));
    }
    
    @Test
    void countShouldInvokeStudentCrudCountMethodAndReturnItsIntegerResult() {
        when(studentCrud.count()).thenReturn(200);
        
        assertThat(studentCrud.count(), equalTo(200));
        verify(studentCrud).count();
    }
}

