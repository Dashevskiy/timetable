package ru.com.dashevskiy.university.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ru.com.dashevskiy.university.service.StudentService;
import ru.com.dashevskiy.university.service.impl.StudentServiceImpl;

public class DependencyInjectionManyalTests {
    
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = 
                new AnnotationConfigApplicationContext(SpringConfiguration.class);
        
        StudentService studentService = context.getBean(StudentServiceImpl.class);
        studentService.findAll(1, 1);
    }
}
