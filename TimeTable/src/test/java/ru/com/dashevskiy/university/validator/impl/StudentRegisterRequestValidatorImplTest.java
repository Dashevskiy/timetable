package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.apache.commons.validator.routines.EmailValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.passay.PasswordValidator;
import org.passay.RuleResult;

import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.entity.user.Student;

@ExtendWith(MockitoExtension.class)
class StudentRegisterRequestValidatorImplTest {
    
    @Mock
    private StudentCrud studentCrud;
    
    @Mock
    private EmailValidator emailValidator;
    
    @Mock
    private PasswordValidator passwordValidator;
    
    @InjectMocks
    private StudentRegisterRequestValidatorImpl validator;    
    
    @Test
    void validateEmailIsUniqueShuoldThrowExeptionIfPassNotUniqueEmail() {        
        when(studentCrud.findStudentByEmail("email"))
                .thenReturn(Optional.of(Student.builder().build()));
        
        assertThrows(IllegalArgumentException.class, 
                () -> validator.validateEmailIsUnique("email"), 
                "Email is not unique.");
    }
    
    @Test
    void validateShouldInvokeAllValidationMethodsWithRightArguments() {        
        StudentRegisterRequest studentRegisterRequest = StudentRegisterRequest.builder()
                                                        .withEmail("email")
                                                        .withPasswordFirstTry("passwordfirst")
                                                        .withPasswordSecondTry("passwordsecond")
                                                        .build();
        
        validator = spy(validator);
        doNothing().when(validator).validateNotNull("email", "Email is null");
        doNothing().when(validator).validateNotNull("passwordfirst", "First try password is null");
        doNothing().when(validator).validateNotNull("passwordsecond", "Second try password is null");
        doNothing().when(validator).validateEmailIsUnique("email");
        doNothing().when(validator).validateEmailFormat("email");
        doNothing().when(validator).validatePasswordsAreEqual("passwordfirst", "passwordsecond");
        doNothing().when(validator).validatePasswordFormat("passwordfirst");        
        validator.validate(studentRegisterRequest);
        
        verify(validator).validateNotNull("email", "Email is null");
        verify(validator).validateNotNull("passwordfirst", "First try password is null");
        verify(validator).validateNotNull("passwordsecond", "Second try password is null");
        verify(validator).validateEmailIsUnique("email");
        verify(validator).validateEmailFormat("email");
        verify(validator).validatePasswordsAreEqual("passwordfirst", "passwordsecond");
        verify(validator).validatePasswordFormat("passwordfirst");        
    }
    
    @Test
    void validateEmailIsUniqueShuoldNotThrowExeptionIfPassUniqueEmail() {        
        when(studentCrud.findStudentByEmail("email")).thenReturn(Optional.empty());
        
        assertDoesNotThrow(() -> validator.validateEmailIsUnique("email"));                
    }
    
    @Test
    void validateEmailFormatThrowsExeptionWhenWrongEmailPassed() {
        when(emailValidator.isValid("email")).thenReturn(false);
        
        assertThrows(IllegalArgumentException.class, 
                     () -> validator.validateEmailFormat("email"),
                     "Wrong email format.");
    }
    
    @Test
    void validateEmailFormatDoesNotThrowsExeptionWhenFineEmailPassed() {
        when(emailValidator.isValid("email")).thenReturn(true);
        
        assertDoesNotThrow(() -> validator.validateEmailFormat("email"));        
    }
    
    @Test
    void validatePasswordFormatShuldThrowAnExeptionWhenGetsPasswordWithWrongFormat() {        
        RuleResult ruleResult = mock(RuleResult.class);
        when(passwordValidator.validate(any())).thenReturn(ruleResult);
        when(ruleResult.isValid()).thenReturn(false);        
        
        assertThrows(IllegalArgumentException.class, 
                     () -> validator.validatePasswordFormat("password"),
                     "Wrong password error.");
    }
    
    @Test
    void validatePasswordFormatShuldNotThrowAnExeptionWhenGetsPasswordWithFineFormat() {        
        RuleResult ruleResult = mock(RuleResult.class);
        when(passwordValidator.validate(any())).thenReturn(ruleResult);
        when(ruleResult.isValid()).thenReturn(true);        
        
        assertDoesNotThrow(() -> validator.validatePasswordFormat("password"));                     
    }
    
    @Test
    void validatePasswordsAreEqualShouldThrowExeptionIfGetsTwoDifferentPasswords() {
        assertThrows(IllegalArgumentException.class, 
                     () -> validator.validatePasswordsAreEqual("password", "another password"),
                     "Passwords are not equal.");
    }
    
    @Test
    void validatePasswordsAreEqualShouldNotThrowAnExeptionWhenTwoEqualPasswordsPassed() {
        assertDoesNotThrow(() -> validator.validatePasswordsAreEqual("password", "password"));
    }
}
