package ru.com.dashevskiy.university.dao.crudimpl;

import org.junit.jupiter.api.Test;

import ru.com.dashevskiy.university.entity.Course;

class CourseCrudImplTest extends CrudDaoTest<Course> {    
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {        
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(courseCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {        
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(courseCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {        
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(courseCrud);
    }
        
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(courseCrud);
    }
    
    @Test    
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(courseCrud);
    }
    
    @Test    
    void updateShouldUpdateObjectInDatabaseWhenPassValidGroup() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(courseCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(courseCrud);
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(courseCrud);
    }
    
    @Override
    protected Course getTestObject(int id) {        
        return new Course(id, "description", "name");
    } 
    
    @Override
    protected Course getAnotherTestObject(int id) {        
        return new Course(id, "another description", "another name");
    }
}
