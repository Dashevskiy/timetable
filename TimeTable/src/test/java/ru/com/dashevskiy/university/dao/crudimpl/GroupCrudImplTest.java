package ru.com.dashevskiy.university.dao.crudimpl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import ru.com.dashevskiy.university.entity.Group;
import ru.com.dashevskiy.university.entity.user.Student;

class GroupCrudImplTest extends CrudDaoTest<Group> {    
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(groupCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(groupCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(groupCrud);
    }
    
    @Test
    void findByNameShouldReturnEmptyOptionalWhenGetsNameOfNonExistingGroup() {
        groupCrud.save(getTestObject(1));
        
        assertThat(groupCrud.findByName("another name").isPresent(), equalTo(false));
    }
    
    @Test
    void findByNameShouldReturnOptionalWtinWhenGetsNameOfExistingGroup() {
        Group tetsGroup = getTestObject(1);
        groupCrud.save(tetsGroup);
        groupCrud.save(getAnotherTestObject(1));
        
        assertThat(groupCrud.findByName("name").get(), samePropertyValuesAs(tetsGroup));
    }
        
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(groupCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(groupCrud);
    }
    
    @Test
    void updateShouldUpdateObjectInDatabaseWhenPassValidGroup() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(groupCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(groupCrud);
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(groupCrud);
    }
    
    @Test
    void assignStudentsToGroupShouldAddStudentsToGroupWhenGetsAppropriateIds() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        List<Integer> studentsIds = new ArrayList<>();
        studentsIds.add(1);
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(Student.builder()
                            .withId(1)
                            .withEmail("email")
                            .withPassword("$2a$10$trT3.R/Nfey62eczbKEnueTcIbJXW.u1ffAo/XfyLpofwNDbEB86O")
                            .withFirstname("firstname")
                            .withSecondname("secondname") 
                            .withStudentRole(Student.Role.REGULAR)
                            .build());  
        
        groupCrud.assignStudentsToGroup(2, studentsIds);
        
        assertAll(() -> assertThat(studentCrud.findStudentsByGroupId(2), equalTo(studentList)),
                  () -> assertThat(studentCrud.findStudentsByGroupId(2).get(0), samePropertyValuesAs(studentList.get(0))));
    }
    
    @Test
    void removeStudentsFromGroupShouldRemoveLastGroupAssighitionWhenGetsAppropriateIds() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        List<Integer> studentIds = new ArrayList<>();
        studentIds.add(1);
        groupCrud.removeStudentsFromGroup(1, studentIds);
        
        assertThat(studentCrud.findStudentsByGroupId(1).isEmpty(), equalTo(true));
    }
    
    @Test
    void findGroupsByLessonIdShouldReturnGroupAssociatedWithGivenLessonId() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        List<Group> groups = new ArrayList<Group>();
        groups.add(getTestObject(1));
        
        assertAll(() -> assertThat(groupCrud.findByLessonId(1), equalTo(groups)),
                  () -> assertThat(groupCrud.findByLessonId(1).get(0), samePropertyValuesAs(groups.get(0))));
    }

    @Override
    protected Group getTestObject(int id) {
        return new Group(id, false, "name");
    }

    @Override
    protected Group getAnotherTestObject(int id) {
        return new Group(id, false, "another name");
    }
}
