package ru.com.dashevskiy.university.service.implementation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;
import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.entity.Group;
import ru.com.dashevskiy.university.entity.user.Student;
import ru.com.dashevskiy.university.service.GroupService;
import ru.com.dashevskiy.university.service.impl.GroupServiceImpl;
import ru.com.dashevskiy.university.service.mapper.GroupAddRequestMapper;
import ru.com.dashevskiy.university.service.mapper.GroupResponseMapper;
import ru.com.dashevskiy.university.service.mapper.GroupUpdateRequestMapper;
import ru.com.dashevskiy.university.service.mapper.StudentMapper;
import ru.com.dashevskiy.university.validator.Validator;

@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {
    
    @Mock
    StudentCrud studentCrud;
    
    @Mock
    GroupCrud groupCrud;
    
    @Mock
    StudentMapper studentMapper;
    
    @Mock
    GroupAddRequestMapper groupAddRequestMapper;
    
    @Mock
    GroupUpdateRequestMapper groupUpdateRequestMapper;
    
    @Mock
    GroupResponseMapper groupResponseMapper;
    
    @Mock
    Validator<GroupAddRequest> groupAddRequestVallidator;
    
    @Mock
    Validator<GroupUpdateRequest> groupUpdateRequestVallidator;
    
    @Mock
    Validator<GroupStudenAssignationRequest> groupStudenAssignationRequestValidator;    
    
    @Mock
    GroupService groupService;
    
    @BeforeEach
    private void createMocks() {        
        groupService = new GroupServiceImpl(studentCrud, 
                                                groupCrud, 
                                                studentMapper, 
                                                groupAddRequestMapper, 
                                                groupUpdateRequestMapper, 
                                                groupResponseMapper, 
                                                groupAddRequestVallidator, 
                                                groupUpdateRequestVallidator, 
                                                groupStudenAssignationRequestValidator);
    }
    
    @Test
    void addShouldInvokeValidatorAndOtherMethodsInAppropriateOrder() {
        GroupAddRequest groupAddRequest = new GroupAddRequest();
        Group group = new Group();
        InOrder inOrder = inOrder(groupAddRequestVallidator, groupAddRequestMapper, groupCrud);
        when(groupAddRequestMapper.mapEntityToDomain(groupAddRequest)).thenReturn(group);
        groupService.add(groupAddRequest);
        
        inOrder.verify(groupAddRequestVallidator).validate(groupAddRequest);
        inOrder.verify(groupAddRequestMapper).mapEntityToDomain(groupAddRequest);
        inOrder.verify(groupCrud).save(group);        
    }
    
    @Test
    void findShuldThrowAnExeptionIfNotExistingGroupIdPasse() {
        when(groupCrud.findById(1)).thenReturn(Optional.empty());
        
        assertThrows(NoSuchElementException.class,
                     () -> groupService.find(1),
                     "Did not find group with such id - 1");                     
    }
    
    @Test
    void findShuldInvokeMethodsInAppropriateOrder() {        
        Group group = new Group(1, false, "name");
        when(groupCrud.findById(1)).thenReturn(Optional.of(group));        
        InOrder inOrder = inOrder(groupCrud, groupResponseMapper);        
        groupService.find(1);
        
        inOrder.verify(groupCrud).findById(1) ;
        inOrder.verify(groupResponseMapper).mapDomainToEntity(group);
                           
    }
    
    @Test
    void findAllShuldInvokeMethodsInAppropriateOrder() {        
        List<Group> groups = new ArrayList<>();
        groups.add(new Group(1, false, "name"));
        groups.add(new Group(2, false, "another name"));
        when(groupCrud.findAll(1, 2)).thenReturn(groups);        
        InOrder inOrder = inOrder(groupCrud, groupResponseMapper);        
        groupService.findAll(1, 2);
        
        inOrder.verify(groupCrud).findAll(1, 2) ;
        inOrder.verify(groupResponseMapper, times(2)).mapDomainToEntity(any());
                           
    }
    
    @Test
    void updateShouldInvokeValidatorAndOtherMethodsInAppropriateOrder() {
        GroupUpdateRequest groupUpdateRequest = new GroupUpdateRequest();
        Group group = new Group();
        InOrder inOrder = inOrder(groupUpdateRequestVallidator, groupUpdateRequestMapper, groupCrud);
        when(groupUpdateRequestMapper.mapEntityToDomain(groupUpdateRequest)).thenReturn(group);
        groupService.update(groupUpdateRequest);
        
        inOrder.verify(groupUpdateRequestVallidator).validate(groupUpdateRequest);
        inOrder.verify(groupUpdateRequestMapper).mapEntityToDomain(groupUpdateRequest);
        inOrder.verify(groupCrud).update(group);        
    }
    
    @Test
    void assighStudentsToGroupShouldInvokeValidatorAndOtherMethodsInAppropriateOrder() {        
        Set<Integer> studentIds = new HashSet<>();
        studentIds.add(1);
        GroupStudenAssignationRequest assignationRequest = new GroupStudenAssignationRequest(studentIds, 1);
        
        InOrder inOrder = inOrder(groupStudenAssignationRequestValidator, groupCrud);        
        groupService.assighStudentsToGroup(assignationRequest);
        
        inOrder.verify(groupStudenAssignationRequestValidator).validate(assignationRequest);        
        inOrder.verify(groupCrud).assignStudentsToGroup(1, new ArrayList<>(studentIds));      
    }
    
    @Test
    void removeStudentFromGroupShouldInvokeCrudMethod() {
        Set<Integer> studentIds = new HashSet<>();
        studentIds.add(1);
        
        groupService.removeStudentFromGroup(studentIds, 1);
        verify(groupCrud).removeStudentsFromGroup(1, new ArrayList<>(studentIds));
    }
    
    @Test
    void getStudentsShouldInvokeMethodsInAppropriateOrder() {
        List<Student> students = new ArrayList<>();
        students.add(Student.builder().withId(1).build());
        when(studentCrud.findStudentsByGroupId(1)).thenReturn(students);        
        InOrder inOrder = inOrder(studentCrud, studentMapper);
        groupService.getStudents(1);        
        
        inOrder.verify(studentCrud).findStudentsByGroupId(1);
        inOrder.verify(studentMapper).mapEntityToDomain(any());
    }
    
    @Test
    void countShouldInvokeGroupCrudCountMethodAndReturnItsIntegerResult() {
        when(groupCrud.count()).thenReturn(200);
        
        assertThat(groupCrud.count(), equalTo(200));
        verify(groupCrud).count();
    }
}
