package ru.com.dashevskiy.university.dao.crudimpl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import ru.com.dashevskiy.university.entity.user.Student;

class StudentCrudImplTest extends CrudDaoTest<Student> {
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(studentCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(studentCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(studentCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(studentCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(studentCrud);
    }
    
    @Test
    void findStudentsByGroupIdShouldReturnListOfStudentsWhenPassGroupId() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(getTestObject(1));
        
        assertAll(() -> assertThat(studentCrud.findStudentsByGroupId(1), equalTo(studentList)),
                  () -> assertThat(studentCrud.findStudentsByGroupId(1).get(0), samePropertyValuesAs(studentList.get(0))));
    }
    
    @Test
    void updateShouldUpdateObjectInDatabaseWhenPassValidObject() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(studentCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(studentCrud);
    }
    
    @Test
    void findStudentByEmailShouldReturnOptionalWithExistingStudentWithGivenEmail () {
        Student testStudent = getTestObject(1);
        studentCrud.save(testStudent);
        
        assertThat(studentCrud.findStudentByEmail("email").get(), samePropertyValuesAs(testStudent));
    }
    
    @Test
    void findStudentByEmailShouldReturnEmptyOptionalWhenGetsStudtnWithAbsentEmail () {
        Student testStudent = getTestObject(1);
        studentCrud.save(testStudent);
        
        assertThat(studentCrud.findStudentByEmail("emailoooo").isPresent(), equalTo(false));
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(studentCrud);
    }
    
    @Override
    protected Student getTestObject(int id) {
        return Student.builder()
                .withId(id)
                .withEmail("email")
                .withPassword("$2a$10$trT3.R/Nfey62eczbKEnueTcIbJXW.u1ffAo/XfyLpofwNDbEB86O")
                .withFirstname("firstname")
                .withSecondname("secondname") 
                .withStudentRole(Student.Role.REGULAR)
                .build();
    } 

    @Override
    protected Student getAnotherTestObject(int id) {
        return Student.builder()
                .withId(id)
                .withEmail("another email")
                .withPassword("$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2")
                .withFirstname("another firstname")
                .withSecondname("another secondname") 
                .withStudentRole(Student.Role.LIBRARIAN)
                .build();
    }
}
