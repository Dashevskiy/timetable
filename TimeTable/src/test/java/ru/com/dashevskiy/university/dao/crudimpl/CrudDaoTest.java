package ru.com.dashevskiy.university.dao.crudimpl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import ru.com.dashevskiy.university.dao.AuditoryCrud;
import ru.com.dashevskiy.university.dao.CourseCrud;
import ru.com.dashevskiy.university.dao.CrudDao;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.dao.InCampusAdressCrud;
import ru.com.dashevskiy.university.dao.LessonCrud;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.dao.TeacherCrud;
import ru.com.dashevskiy.university.dao.impl.AuditoryCrudImpl;
import ru.com.dashevskiy.university.dao.impl.CourseCrudImpl;
import ru.com.dashevskiy.university.dao.impl.GroupCrudImpl;
import ru.com.dashevskiy.university.dao.impl.InCampusAdressCrudImpl;
import ru.com.dashevskiy.university.dao.impl.LessonCrudImpl;
import ru.com.dashevskiy.university.dao.impl.StudentCrudImpl;
import ru.com.dashevskiy.university.dao.impl.TeacherCrudImpl;

abstract class CrudDaoTest <T> {
    protected static EmbeddedDatabase embeddedDatabase;
    protected static SimpleJdbcInsert jdbcInsert;
    protected static NamedParameterJdbcTemplate jdbcTemplate;
    protected static InCampusAdressCrud inCampusAdressCrud;
    protected static AuditoryCrud auditoryCrud;
    protected static CourseCrud courseCrud;
    protected static TeacherCrud teacherCrud;
    protected static StudentCrud studentCrud;
    protected static GroupCrud groupCrud;
    protected static LessonCrud lessonCrud;
    protected static NamedParameterJdbcTemplate testJdbcTemplate;

    @BeforeEach
    private void setUp() {
        embeddedDatabase = new EmbeddedDatabaseBuilder()
                .setName("testdb;DB_CLOSE_DELAY=-1;MODE=PostgreSQL;")
                .setType(H2)
                .addScript("schema.sql")
                .build();

        testJdbcTemplate = new NamedParameterJdbcTemplate(embeddedDatabase);

        studentCrud = new StudentCrudImpl(testJdbcTemplate);
        inCampusAdressCrud = new InCampusAdressCrudImpl(testJdbcTemplate);
        auditoryCrud = new AuditoryCrudImpl(testJdbcTemplate);
        courseCrud = new CourseCrudImpl(testJdbcTemplate);
        teacherCrud = new TeacherCrudImpl(testJdbcTemplate);
        groupCrud = new GroupCrudImpl(testJdbcTemplate);
        lessonCrud = new LessonCrudImpl(testJdbcTemplate);
    };

    @AfterAll
    public static void tearDown() {
        embeddedDatabase.shutdown();
    }

    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(CrudDao<T> dao) {
        T testObject = getTestObject(1);
        
        assertAll(() -> assertThat(dao.save(testObject), equalTo(1)),
                  () -> assertThat(dao.findById(1).get(), samePropertyValuesAs(testObject)));
    }

    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(CrudDao<T> dao) {
        T testObject = getTestObject(1);
        int objectId = dao.save(testObject);
        
        assertThat(dao.findById(objectId).get(), samePropertyValuesAs(testObject));
    }

    void findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(CrudDao<T> dao) {
        assertThat(dao.findById(1).isPresent(), equalTo(false));
    }

    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(CrudDao<T> dao) {
        List<T> testObjects = new ArrayList<>();
        testObjects.add(getTestObject(1));
        testObjects.add(getAnotherTestObject(2));
        testObjects.forEach(dao::save);
        
        assertThat(dao.findAll(), samePropertyValuesAs(testObjects));
    }

    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(CrudDao<T> dao) {
        final T firstObject = getTestObject(1);
        dao.save(firstObject);
        final T secondObject = getAnotherTestObject(2);
        dao.save(secondObject);
        
        List<T> firstPageObjects = new ArrayList<>();
        firstPageObjects.add(firstObject);        
        
        List<T> secondPageObjects = new ArrayList<>();
        secondPageObjects.add(secondObject);              
        
        assertAll(() -> assertThat(dao.findAll(0, 1), equalTo(firstPageObjects)),
                  () -> assertThat(dao.findAll(0, 1).get(0), samePropertyValuesAs(firstObject)),
                  () -> assertThat(dao.findAll(1, 1), samePropertyValuesAs(secondPageObjects)),
                  () -> assertThat(dao.findAll(1, 1).get(0), samePropertyValuesAs(secondObject)));
    }

    void updateShouldUpdateObjectInDatabaseWhenPassValidObject(CrudDao<T> dao) {
        dao.save(getTestObject(1));
        T updateObject = getAnotherTestObject(1);
        dao.update(updateObject);
        
        assertThat(dao.findById(1).get(), samePropertyValuesAs(updateObject));        
    }

    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId(CrudDao<T> dao) {
        dao.save(getTestObject(1));
        dao.deleteById(1);
        
        assertThat(dao.findAll().isEmpty(), equalTo(true));
    }

    void countShouldReturnAmountOfRowsInEntityTable(CrudDao<T> dao) {
        dao.save(getTestObject(1));
        dao.save(getAnotherTestObject(2));
        
        assertThat(dao.count(), equalTo(2));
    }

    protected abstract T getTestObject(int id);

    protected abstract T getAnotherTestObject(int id);    
}
