package ru.com.dashevskiy.university.validator.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;
import ru.com.dashevskiy.university.entity.user.Student;

@ExtendWith(MockitoExtension.class)
class StudentUpdatePersonalDataRequestValidatorImplTest {
    
    @Mock
    private StudentCrud studentCrud;
    
    @InjectMocks
    private StudentUpdatePersonalDataRequestValidatorImpl validator;    
    
    @Test
    void validateShouldInvokeAllValidationMethodsWithRightArguments() {        
        StudentUpdatePersonalDataRequest studentRegisterRequest 
                = StudentUpdatePersonalDataRequest.builder()
                  .withId(1)
                  .withFirstname("firstname")
                  .withSecondname("secondname")
                  .build();
        
        validator = spy(validator);       
        doNothing().when(validator).validateStudentExists(1);
        doNothing().when(validator).validateNotNull("firstname", "Firstname is null");
        doNothing().when(validator).validateNotNull("secondname", "Secondname is null");
        doNothing().when(validator).validateNotEmpty("firstname", "Firstname is empty");
        doNothing().when(validator).validateNotEmpty("secondname", "Secondname is empty");         
        validator.validate(studentRegisterRequest);
        
        verify(validator).validateStudentExists(1);
        verify(validator).validateNotNull("firstname", "Firstname is null");
        verify(validator).validateNotNull("secondname", "Secondname is null");
        verify(validator).validateNotEmpty("firstname", "Firstname is empty");
        verify(validator).validateNotEmpty("secondname", "Secondname is empty");       
    }
    
    @Test
    void validateStudentExistsShouldThrowExeptionIfNoStudetnWithSuchIdExtistsInDatabase() {
        when(studentCrud.findById(1)).thenReturn(Optional.empty());
        
        assertThrows(NoSuchElementException.class, 
                     () -> validator.validateStudentExists(1));
    }
    
    @Test
    void validateStudentExistsShouldNotThrowExeptionIfStudetnWithSuchIdExtistsInDatabase() {
        when(studentCrud.findById(1)).thenReturn(Optional.of(Student.builder().build()));
        
        assertDoesNotThrow(() -> validator.validateStudentExists(1));
    }
}
