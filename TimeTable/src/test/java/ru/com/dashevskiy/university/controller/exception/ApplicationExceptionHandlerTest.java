package ru.com.dashevskiy.university.controller.exception;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import ru.com.dashevskiy.university.controller.AbstractControllerTest;
import ru.com.dashevskiy.university.controller.ApplicationController;

class ApplicationExceptionHandlerTest extends AbstractControllerTest {

    @MockBean
    ApplicationController controller;

    @Test
    void handleExeptionShouldReturn500CodeWhenUnhandledExeptionThrowed() throws Exception {
        when(controller.goHome()).thenThrow(new RuntimeException("Some exception"));
        
        mockMvc.perform(get("/"))
               .andExpect(status().isInternalServerError());
    }

    @Test
    void handleExeptionShouldReturnErrorViewhenUnhandledExeptionThrowed() throws Exception {
        when(controller.goHome()).thenThrow(new RuntimeException("Some exception"));
        
        mockMvc.perform(get("/"))
               .andExpect(view().name("error"));;
    }

    @Test
    void handleExeptionShouldReturnModelWithUrlValueWhenUnhandledExeptionThrowed() throws Exception {
        Exception exception = new RuntimeException("Some exception");
        when(controller.goHome()).thenThrow(exception);
        
        mockMvc.perform(get("/"))
               .andExpect(model().attribute("url", equalTo("/")));
    }

    @Test
    void handleExeptionShouldReturnModelWithTimestampValueWhenUnhandledExeptionThrowed() throws Exception {
        Exception exception = new RuntimeException("Some exception");
        when(controller.goHome()).thenThrow(exception);
        
        mockMvc.perform(get("/"))
               .andExpect(model().attributeExists("timestamp"));
    }

    @Test
    void handleExeptionShouldReturnModelWithPageStatusValueWhenUnhandledExeptionThrowed() throws Exception {
        Exception exception = new RuntimeException("Some exception");
        when(controller.goHome()).thenThrow(exception);
        
        mockMvc.perform(get("/"))
               .andExpect(model().attribute("status", equalTo(HttpStatus.INTERNAL_SERVER_ERROR)));
    }
}
