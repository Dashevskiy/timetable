package ru.com.dashevskiy.university.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import ru.com.dashevskiy.university.config.SpringConfiguration;


@WebAppConfiguration
@WebMvcTest
@ContextConfiguration(classes = SpringConfiguration.class)
@ExtendWith(SpringExtension.class)
public abstract class AbstractControllerTest {
    
    @Autowired
    protected WebApplicationContext context;
    protected MockMvc mockMvc;

    @BeforeEach
    public void initalize() {
        mockMvc = MockMvcBuilders
                  .webAppContextSetup(context)
                  .build();
    }
}
