package ru.com.dashevskiy.university.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import ru.com.dashevskiy.university.service.StudentService;

class StudentControllerTest extends AbstractControllerTest {

    @MockBean
    private StudentService studentService;
    
    @Autowired
    private StudentController controller;
    
    @Value("${students.controller.itemsOnPage}")
    private Integer itemPerPage;
    
    @Value("${students.controller.defaultPage}")
    private Integer defaultPage;
    
    @BeforeEach
    public void initalize() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setSuffix(".html");
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setViewResolvers(viewResolver)
                .build();
    }
    
    @Test
    void performingStudentsCallShouldCallStudentsTemplateWithOkStatus() throws Exception {
        mockMvc.perform(get("/students").param("page", "0"))
            .andExpect(status().isOk())
            .andExpect(view().name("students"));
    }
    
    @Test
    void StudentsCallModelShuldConntainParameterWithPassedPageParrameter() throws Exception {
        when(studentService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/students").param("page", "5"))
            .andExpect(model().attribute("page", equalTo(5)));
    }
    
    @Test
    void performingStudentsCallShouldCallFindAllMethodFromStudetnServiceWithAppropriateArguments() throws Exception {
        when(studentService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/students").param("page", "5"));
        
        verify(studentService).findAll(4, itemPerPage);
    }
    
    @Test
    void performingStudentsCallShouldReturnDefaultPageWhenCalledWithoutParameter() throws Exception {
        when(studentService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/students"));
        
        verify(studentService).findAll(defaultPage - 1, itemPerPage);
    }
    
    @Test
    void performingStudentsCallShouldReturnDefaultPageWhenCalledWithPageLessThanOne() throws Exception {
        when(studentService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/students").param("page", "0"));
        
        verify(studentService).findAll(defaultPage - 1, itemPerPage);
    }
    
    @Test
    void performingStudentsCallShouldReturnDefaultPageWhenCalledWithPageBiggerThanTotalPages() throws Exception {
        when(studentService.count()).thenReturn(10 * itemPerPage);
        mockMvc.perform(get("/students").param("page", "12"));
        
        verify(studentService).findAll(10, itemPerPage);
    }
}
