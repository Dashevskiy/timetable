package ru.com.dashevskiy.university.dao.crudimpl;

import org.junit.jupiter.api.Test;

import ru.com.dashevskiy.university.entity.InCampusAdress;

class InCampusAdressCrudImplTest extends CrudDaoTest<InCampusAdress> {
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(inCampusAdressCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(inCampusAdressCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(inCampusAdressCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(inCampusAdressCrud);
    }
    
    @Test
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(inCampusAdressCrud);
    }
    
    @Test
    void updateShouldUpdateObjectInDatabaseWhenPassValidGroup() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(inCampusAdressCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(inCampusAdressCrud);
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(inCampusAdressCrud);
    }
    
    @Override
    protected InCampusAdress getTestObject(int id) {
        return InCampusAdress.builder()
                .withId(id)
                .withBuilding("building")
                .withEntry(1)
                .withFloor(1)
                .build();
    }
    
    @Override
    protected InCampusAdress getAnotherTestObject(int id) {
        return InCampusAdress.builder()
                .withId(id)
                .withBuilding("another building")
                .withEntry(2)
                .withFloor(2)
                .build();
    }
}
