package ru.com.dashevskiy.university.dao.crudimpl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import ru.com.dashevskiy.university.entity.Auditory;
import ru.com.dashevskiy.university.entity.Course;
import ru.com.dashevskiy.university.entity.Group;
import ru.com.dashevskiy.university.entity.InCampusAdress;
import ru.com.dashevskiy.university.entity.Lesson;
import ru.com.dashevskiy.university.entity.user.Teacher;

class LessonCrudImplTest extends CrudDaoTest<Lesson> {
    
    @Test
    void saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed() {
        saveShouldAddRecordInDatabaseAndReturnIDWhenValidObjectPassed(lessonCrud);
    }
    
    @Test
    void findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord() {
        findByIdShouldReturnDomainbjectIfPassIdOfExistingRecord(lessonCrud);
    }
    
    @Test
    void findByIdThrowExeptionIfPassIdOfNonExistingRecord() {
        findByIdShouldReturnEmptyOptionalIfPassIdOfNonExistingRecord(lessonCrud);
    }
        
    @Test
    void findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked() {
        findAllShouldReturnCollectionWithAllObjectsStoredInTableWhenInvoked(lessonCrud);
    }
    
    @Test    
    void findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed() {
        findAllShouldReturnCollectionWithFromChosenPageWhenValidPaginationParamsPassed(lessonCrud);
    }
    
    @Test
    void updateShouldUpdateObjectInDatabaseWhenPassValidGroup() {
        updateShouldUpdateObjectInDatabaseWhenPassValidObject(lessonCrud);
    }
    
    @Test
    void deleteByIdshuoldDeleteRecordByIdWhenPassValidId() {
        deleteByIdshuoldDeleteRecordByIdWhenPassValidId(lessonCrud);
    }
    
    @Test
    void assignGroupsToLessonShuoldAddGroupToPassedLessonById() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        List<Integer> groupsIds = new ArrayList<>();
        groupsIds.add(2);
        List<Group> groups = new ArrayList<>();
        groups.add(new Group(2, false, "another name"));
        
        lessonCrud.assignGroupsToLesson(2, groupsIds);
        
        assertAll(() -> assertThat(groupCrud.findByLessonId(2), equalTo(groups)),
                  () -> assertThat(groupCrud.findByLessonId(2).get(0), samePropertyValuesAs(groups.get(0))));
    }
    
    @Test
    void removeGroupsFromLessonShouldRemoveGroupsFromGivenLessonById() throws ScriptException, SQLException {
        ScriptUtils.executeSqlScript(embeddedDatabase.getConnection(), new ClassPathResource("data.sql"));
        List<Integer> groupIds = new ArrayList<>();
        groupIds.add(1);
        lessonCrud.removeGroupsFromLesson(1, groupIds);
        
        assertThat(groupCrud.findByLessonId(1).isEmpty(), equalTo(true));
    }
    
    @Test
    void countShouldReturnAmountOfRowsInEntityTable() {
        countShouldReturnAmountOfRowsInEntityTable(lessonCrud);
    }

    @Override
    protected Lesson getTestObject(int id) {
        Integer courseId = courseCrud.save(new Course(1, "description", "name"));
        Integer teacherId = teacherCrud.save(Teacher.builder().build());
        Integer addressId = inCampusAdressCrud.save(InCampusAdress.builder().build());
        Integer auditoryId = auditoryCrud.save(new Auditory(1, addressId, "name"));
        
        return Lesson.builder()
                .withId(id)
                .withStartDate(LocalDateTime.now())
                .withEndDate(LocalDateTime.now())
                .withAuditoryId(auditoryId)
                .withCourseId(courseId)
                .withTeacherId(teacherId)
                .build();
    }

    @Override
    protected Lesson getAnotherTestObject(int id) {
        Integer courseId = courseCrud.save(new Course(1, "description", "name"));
        Integer teacherId = teacherCrud.save(Teacher.builder().build());
        Integer addressId = inCampusAdressCrud.save(InCampusAdress.builder().build());
        Integer auditoryId = auditoryCrud.save(new Auditory(1, addressId, "name"));
        
        return Lesson.builder()
                .withId(id)
                .withStartDate(LocalDateTime.now().minusMonths(1))
                .withEndDate(LocalDateTime.now().minusMonths(1))
                .withAuditoryId(auditoryId)
                .withCourseId(courseId)
                .withTeacherId(teacherId)
                .build();
    }
}
