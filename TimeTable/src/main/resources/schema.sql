DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS user_statuses CASCADE;
DROP TABLE IF EXISTS groups CASCADE;
DROP TABLE IF EXISTS groups_to_sutdents_assigments CASCADE;
DROP TABLE IF EXISTS courses CASCADE;
DROP TABLE IF EXISTS in_campus_addresses CASCADE;
DROP TABLE IF EXISTS auditories CASCADE;
DROP TABLE IF EXISTS lessons CASCADE;
DROP TABLE IF EXISTS lessons_to_groups_assigments CASCADE;

CREATE TABLE user_statuses(
	id SERIAL PRIMARY KEY,
	name VARCHAR(10)
);

INSERT INTO user_statuses (name) VALUES ('new'), ('assigned'), ('archive');

CREATE TABLE users(
	id SERIAL PRIMARY KEY,
	status_id INTEGER REFERENCES user_statuses(id),
	inactive BOOLEAN,
	class_mark VARCHAR (10),
	email VARCHAR (320) UNIQUE,	
	password CHAR (60),
	firstname VARCHAR (50),
	secondname VARCHAR (50),
	student_role VARCHAR (100),
	dossier	TEXT
);

CREATE TABLE groups(
	id SERIAL PRIMARY KEY,
	inactive BOOLEAN,
	name VARCHAR (50) UNIQUE
);

CREATE TABLE groups_to_sutdents_assigments(	
	student_id INTEGER REFERENCES users(id),
	group_id INTEGER REFERENCES groups(id)
);

CREATE TABLE courses(
	id SERIAL PRIMARY KEY,
	name VARCHAR (50),
	description TEXT
);

CREATE TABLE in_campus_addresses(
	id SERIAL PRIMARY KEY,
	building VARCHAR (50),
	entry INTEGER, 
	floor INTEGER
);

CREATE TABLE auditories(
	id SERIAL PRIMARY KEY,
	address_id INTEGER REFERENCES in_campus_addresses(id),
	name VARCHAR (50)	
);

CREATE TABLE lessons(
	id SERIAL PRIMARY KEY,
	teacher_id INTEGER REFERENCES users(id),
	auditory_id INTEGER REFERENCES auditories(id),
	course_id INTEGER REFERENCES courses(id),
	start_date TIMESTAMP(9),
	end_date TIMESTAMP(9)	
);

CREATE TABLE lessons_to_groups_assigments(	
	lesson_id INTEGER REFERENCES lessons(id),
	group_id INTEGER REFERENCES groups(id)
)