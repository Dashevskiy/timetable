package ru.com.dashevskiy.university.validator.impl;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.validator.GroupAddRequestVallidator;

@Component
@RequiredArgsConstructor
@Slf4j
public class GroupAddRequestVallidatorImpl implements GroupAddRequestVallidator {
    private final GroupCrud groupCrud;    

    @Override
    public void validate(GroupAddRequest groupAddRequest) {
        log.debug("Validating group add request - {}", groupAddRequest);
        log.trace("Not empty and not null checks");
        validateNotNull(groupAddRequest.getName(), "Name is null.");
        validateNotEmpty(groupAddRequest.getName(), "Name is empty.");        
        
        validateNameIsUnique(groupAddRequest.getName());
    }

    void validateNameIsUnique(String name) {
        log.trace("Validating group add request's name - {} is unique", name);
        groupCrud.findByName(name).orElseThrow(
                () -> new IllegalArgumentException("Name is not unique."));        
    }
}
