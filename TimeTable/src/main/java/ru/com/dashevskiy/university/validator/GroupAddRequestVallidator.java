package ru.com.dashevskiy.university.validator;

import ru.com.dashevskiy.university.domain.group.GroupAddRequest;

public interface GroupAddRequestVallidator extends Validator<GroupAddRequest> {
}
