package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.entity.user.Student;

@Mapper(componentModel = "spring")
public interface StudentRegisterRequestMapper {

    @Mapping(source = "encodedPassword", target = "password")
    Student mapDomainToEntity(StudentRegisterRequest domain, String encodedPassword);
    
}
