package ru.com.dashevskiy.university.dao;

import java.util.List;

import ru.com.dashevskiy.university.entity.Lesson;

public interface LessonCrud extends CrudDao<Lesson> {
    
    void assignGroupsToLesson(Integer lessonId, List<Integer> groupsId);
    
    void removeGroupsFromLesson(Integer lessonId, List<Integer> groupsId);    
    
}
