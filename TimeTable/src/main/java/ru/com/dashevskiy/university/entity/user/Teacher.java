package ru.com.dashevskiy.university.entity.user;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class Teacher extends User {    
    
    @EqualsAndHashCode.Exclude 
    String dossier;    
    
    @Builder(setterPrefix = "with", toBuilder = true)
    public Teacher(Integer id,
                   boolean inactive,
                   String email,
                   String password,
                   String firstname, 
                   String secondname, 
                   String dossier) {
        super(id,
              inactive,
              email,
              password,
              firstname, 
              secondname);
        this.dossier = dossier;
    }    
}
