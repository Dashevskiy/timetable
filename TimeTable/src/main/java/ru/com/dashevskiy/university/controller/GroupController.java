package ru.com.dashevskiy.university.controller;

import static ru.com.dashevskiy.university.controller.utility.PaginationUtility.getPageNumberOrDefaultIfNotValid;
import static ru.com.dashevskiy.university.controller.utility.PaginationUtility.getTotalPages;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.RequiredArgsConstructor;
import ru.com.dashevskiy.university.service.GroupService;

@Controller
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;
    
    @Value("${groups.controller.itemsOnPage}")
    private Integer itemPerPage;
    
    @Value("${groups.controller.defaultPage}")
    private Integer defaultPage;
    
    @GetMapping("/groups")
    public String goToGroupsList(@RequestParam(defaultValue = "${groups.controller.defaultPage}") String page, 
                                 Model model) {
        final Integer totalPages = getTotalPages(groupService.count(), itemPerPage);
        final int pageNumber = getPageNumberOrDefaultIfNotValid(page, 
                                                                totalPages, 
                                                                defaultPage);
        
        model.addAttribute("groups", groupService.findAll(pageNumber - 1, itemPerPage));
        model.addAttribute("page", pageNumber);
        model.addAttribute("maxPage", totalPages + 1);
        
        return "groups";
    }
}
