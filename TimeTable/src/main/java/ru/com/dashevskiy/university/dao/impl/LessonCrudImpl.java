package ru.com.dashevskiy.university.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.LessonCrud;
import ru.com.dashevskiy.university.entity.Lesson;

@Slf4j
@Repository
public class LessonCrudImpl extends AbstractCrudDaoImpl<Lesson> implements LessonCrud {
    private static final RowMapper<Lesson> ROW_MAPPER = (resultSet, rowNumber) -> 
            Lesson.builder()
                .withId(resultSet.getInt("id"))
                .withStartDate(resultSet.getTimestamp("start_date").toLocalDateTime())
                .withEndDate(resultSet.getTimestamp("end_date").toLocalDateTime())
                .withAuditoryId(resultSet.getInt("auditory_id"))
                .withCourseId(resultSet.getInt("course_id"))
                .withTeacherId(resultSet.getInt("teacher_id"))
                .build();

    private static final String SAVE_QUERY =
            "INSERT INTO lessons (start_date, end_date, auditory_id, course_id, teacher_id)" +
            "VALUES (:startDate, :endDate, :auditoryId, :courseId, :teacherId)";
    private static final String ASSIGN_GROUPS_TO_LESSON_QUERY = 
            "INSERT INTO lessons_to_groups_assigments(lesson_id, group_id) " +
            "VALUES (:lessonId, :groupId) ";
    private static final String UPDATE_TEACHER_STATUS_ID = 
            "UPDATE users SET status_id = 2 WHERE id = :teacherId";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM lessons WHERE id = :id";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM lessons";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM lessons ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String UPDATE_QUERY = 
            "UPDATE lessons " + 
            "SET " + 
            "    start_date = :startDate, " +
            "    end_date = :endDate, " + 
            "    auditory_id = :auditoryId, " + 
            "    course_id = :courseId, " + 
            "    teacher_id = :teacherId " + 
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM lessons WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM lessons";
    private static final String REMOVE_GROUP_FROM_LESSON_QUERY = 
            "DELETE FROM lessons_to_groups_assigments WHERE lesson_id = :lessonId AND group_id = :groupId";

    public LessonCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id", 
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
     }

    @Override
    public Integer save(Lesson lesson) {
        Integer lessonId = super.save(lesson);
        jdbcTemplate.update(UPDATE_TEACHER_STATUS_ID, 
                            new MapSqlParameterSource("teacherId", lesson.getTeacherId()));
        
        return lessonId;
    }

    @Override
    public void update(Lesson lesson) {        
        super.update(lesson);
        jdbcTemplate.update(UPDATE_TEACHER_STATUS_ID, 
                new MapSqlParameterSource("teacherId", lesson.getTeacherId()));
    }

    @Override
    public void assignGroupsToLesson(Integer lessonId, List<Integer> groupsId) {
        jdbcTemplate.batchUpdate(ASSIGN_GROUPS_TO_LESSON_QUERY, 
                                 getLessonGroupsBathArguments(lessonId, groupsId));   
    }

    @Override
    public void removeGroupsFromLesson(Integer lessonId, List<Integer> groupsId) {
        jdbcTemplate.batchUpdate(REMOVE_GROUP_FROM_LESSON_QUERY, 
                                 getLessonGroupsBathArguments(lessonId, groupsId));        
    }
    
    private SqlParameterSource[] getLessonGroupsBathArguments(Integer lessonId, List<Integer> groupsId) {
        return groupsId.stream()
               .map(groupId -> new MapSqlParameterSource()
                                 .addValue("lessonId", lessonId)
                                 .addValue("groupId", groupId))
               .toArray(SqlParameterSource[]::new);        
    }
}
