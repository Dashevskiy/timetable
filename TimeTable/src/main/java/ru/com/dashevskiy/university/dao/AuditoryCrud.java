package ru.com.dashevskiy.university.dao;

import ru.com.dashevskiy.university.entity.Auditory;

public interface AuditoryCrud extends CrudDao<Auditory> {    
}
