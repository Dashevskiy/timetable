package ru.com.dashevskiy.university.service;

import java.util.List;

import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.domain.student.StudentResponse;
import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;

public interface StudentService {    
    
    boolean login(String email, String password);
    
    void register(StudentRegisterRequest registerRequest);
    
    StudentResponse find(int studentId);
    
    List<StudentResponse> findAll(int page, int itemPerPage);
    
    List<StudentResponse> findStudentsByGroup(int groupId);
    
    void updatePersonalData(StudentUpdatePersonalDataRequest UpdateRequest);
    
    int count();

}
