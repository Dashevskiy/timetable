package ru.com.dashevskiy.university.entity;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
public class Auditory {
    
    @EqualsAndHashCode.Exclude 
    Integer id;
    Integer addressId;
    String name;
}
