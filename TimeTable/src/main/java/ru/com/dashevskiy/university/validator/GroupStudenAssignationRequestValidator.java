package ru.com.dashevskiy.university.validator;

import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;

public interface GroupStudenAssignationRequestValidator extends Validator<GroupStudenAssignationRequest> {
}
