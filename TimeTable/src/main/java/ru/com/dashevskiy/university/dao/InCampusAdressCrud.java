package ru.com.dashevskiy.university.dao;

import ru.com.dashevskiy.university.entity.InCampusAdress;

public interface InCampusAdressCrud extends CrudDao<InCampusAdress> {    
}
