package ru.com.dashevskiy.university.controller;

import static ru.com.dashevskiy.university.controller.utility.PaginationUtility.getPageNumberOrDefaultIfNotValid;
import static ru.com.dashevskiy.university.controller.utility.PaginationUtility.getTotalPages;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.RequiredArgsConstructor;
import ru.com.dashevskiy.university.service.StudentService;

@Controller
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    
    @Value("${students.controller.itemsOnPage}")
    private Integer itemPerPage;
    
    @Value("${students.controller.defaultPage}")
    private Integer defaultPage;

    @GetMapping("/students")
    public String goToStudentList(@RequestParam(defaultValue = "${students.controller.defaultPage}") String page, 
                                  Model model) {
        final Integer totalPages = getTotalPages(studentService.count(), itemPerPage);
        final int pageNumber = getPageNumberOrDefaultIfNotValid(page, 
                                                                totalPages, 
                                                                defaultPage);
        
        model.addAttribute("students", studentService.findAll(pageNumber - 1, itemPerPage));
        model.addAttribute("page", pageNumber);
        model.addAttribute("maxPage", totalPages + 1);
        
        return "students";
    }
}
