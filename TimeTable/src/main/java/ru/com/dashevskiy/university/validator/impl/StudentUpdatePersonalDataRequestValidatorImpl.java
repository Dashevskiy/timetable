package ru.com.dashevskiy.university.validator.impl;

import java.util.NoSuchElementException;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;
import ru.com.dashevskiy.university.validator.StudentUpdatePersonalDataRequestValidator;

@Component
@RequiredArgsConstructor
@Slf4j
public class StudentUpdatePersonalDataRequestValidatorImpl implements StudentUpdatePersonalDataRequestValidator {
    private final StudentCrud studentCrud;    

    @Override
    public void validate(StudentUpdatePersonalDataRequest request) {
        log.debug("Validating student update personal data request - {}", request);
        validateStudentExists(request.getId());        
        
        log.trace("Not empty and not null checks");
        validateNotNull(request.getFirstname(), "Firstname is null");
        validateNotNull(request.getSecondname(), "Secondname is null");
        
        validateNotEmpty(request.getFirstname(), "Firstname is empty");
        validateNotEmpty(request.getSecondname(), "Secondname is empty");        
    }

    void validateStudentExists(Integer id) {
        log.trace("Checking student with id - {} exists", id);
        studentCrud.findById(id).orElseThrow(NoSuchElementException::new);        
    } 
}

