package ru.com.dashevskiy.university.controller.exception;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleExeption(HttpServletRequest request, Exception exception) {
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("url", request.getRequestURI());
        modelAndView.addObject("timestamp", LocalDateTime.now());
        modelAndView.addObject("status", HttpStatus.INTERNAL_SERVER_ERROR);
        
        return modelAndView;
    }
}
