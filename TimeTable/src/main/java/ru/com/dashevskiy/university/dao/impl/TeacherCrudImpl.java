package ru.com.dashevskiy.university.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.TeacherCrud;
import ru.com.dashevskiy.university.entity.user.Teacher;

@Slf4j
@Repository
public class TeacherCrudImpl extends AbstractCrudDaoImpl<Teacher> implements TeacherCrud {
    private static final RowMapper<Teacher> ROW_MAPPER = (resultSet, rowNumber) -> 
            Teacher.builder()
                .withId(resultSet.getInt("id"))
                .withInactive(resultSet.getBoolean("inactive"))
                .withEmail(resultSet.getString("email"))
                .withPassword((resultSet.getString("password")))
                .withFirstname(resultSet.getString("firstname"))
                .withSecondname(resultSet.getString("secondname"))
                .withDossier(resultSet.getString("dossier"))
                .build();

    private static final String SAVE_QUERY = 
            "INSERT INTO users (status_id, inactive, email, password, firstname, secondname, dossier, class_mark)" + 
            "VALUES (1, :inactive, :email, :password, :firstname, :secondname, :dossier, 'teacher')";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM users WHERE id = :id AND class_mark = 'teacher'";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM users WHERE class_mark = 'teacher'";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM users WHERE class_mark = 'teacher' ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String UPDATE_QUERY = 
            "UPDATE users " + 
            "SET " + 
            "    inactive = :inactive, " +
            "    email = :email, " + 
            "    password = :password, " +
            "    firstname = :firstname, " + 
            "    secondname = :secondname, " + 
            "    dossier = :dossier, " + 
            "    class_mark = 'teacher' " +
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM users WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM users WHERE class_mark = 'teacher' ";

    public TeacherCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id",
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
    }
}
