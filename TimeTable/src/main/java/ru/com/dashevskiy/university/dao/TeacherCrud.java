package ru.com.dashevskiy.university.dao;

import ru.com.dashevskiy.university.entity.user.Teacher;

public interface TeacherCrud extends CrudDao<Teacher> {
}
