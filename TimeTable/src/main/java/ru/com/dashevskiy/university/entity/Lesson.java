package ru.com.dashevskiy.university.entity;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@Builder(setterPrefix = "with")
public class Lesson {
    
    @EqualsAndHashCode.Exclude 
    Integer id;
    
    @EqualsAndHashCode.Exclude
    Integer courseId;
    
    @EqualsAndHashCode.Exclude
    Integer teacherId;  
    
    LocalDateTime startDate;
    LocalDateTime endDate;
    Integer auditoryId;    
}
