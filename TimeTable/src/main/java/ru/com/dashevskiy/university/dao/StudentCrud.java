package ru.com.dashevskiy.university.dao;

import java.util.List;
import java.util.Optional;

import ru.com.dashevskiy.university.entity.user.Student;

public interface StudentCrud extends CrudDao<Student> {    
    
    List<Student> findStudentsByGroupId(Integer groupId);
    
    Optional<Student> findStudentByEmail(String email);
    
}
