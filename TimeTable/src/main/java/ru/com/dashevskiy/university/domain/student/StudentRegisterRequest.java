package ru.com.dashevskiy.university.domain.student;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class StudentRegisterRequest {
    private String email;
    
    @ToString.Exclude
    private String passwordFirstTry;

    @ToString.Exclude
    private String passwordSecondTry;
}
