package ru.com.dashevskiy.university.domain.group;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupStudenAssignationRequest {
    private Set<Integer> studentIds;
    private Integer groupId;
}
