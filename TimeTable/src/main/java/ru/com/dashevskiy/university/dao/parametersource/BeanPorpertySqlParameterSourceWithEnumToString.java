package ru.com.dashevskiy.university.dao.parametersource;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

public class BeanPorpertySqlParameterSourceWithEnumToString extends BeanPropertySqlParameterSource{
    
    public BeanPorpertySqlParameterSourceWithEnumToString(Object object) {
        super(object); 
    }

    @Override
    public Object getValue(String paramName) {
        Object value = super.getValue(paramName);
        if (value instanceof Enum) {
            return value.toString();
        }

        return value;
    }
}
