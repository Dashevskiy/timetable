package ru.com.dashevskiy.university.domain.group;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupUpdateRequest {
    private Integer id;
    private boolean inactive;
    private String name;
}
