package ru.com.dashevskiy.university.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.AuditoryCrud;
import ru.com.dashevskiy.university.entity.Auditory;

@Slf4j
@Repository
public class AuditoryCrudImpl extends AbstractCrudDaoImpl<Auditory> implements AuditoryCrud {
    private static final RowMapper<Auditory> ROW_MAPPER = (resultSet, rowNumber) -> 
            new Auditory(resultSet.getInt("id"), 
                         resultSet.getInt("address_id"),
                         resultSet.getString("name"));
    
    private static final String SAVE_QUERY = 
            "INSERT INTO auditories (address_id, name) " + 
            "VALUES (:addressId, :name) ";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM auditories WHERE id = :id";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM auditories";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM auditories ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String UPDATE_QUERY = 
            "UPDATE auditories " + 
            "SET " + 
            "    address_id = :addressId, " + 
            "    name = :name " + 
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM auditories WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM auditories";

    public AuditoryCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id", 
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
     }
}
