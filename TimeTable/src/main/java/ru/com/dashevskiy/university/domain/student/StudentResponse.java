package ru.com.dashevskiy.university.domain.student;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.com.dashevskiy.university.entity.user.Student;

@Data
@Builder(setterPrefix = "with", toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class StudentResponse {    
    private Integer id;    
    private boolean inactive;    
    private String email;
    
    @ToString.Exclude
    private String password;
    private String firstname;
    private String secondname;    
    private Student.Role studentRole;
}
