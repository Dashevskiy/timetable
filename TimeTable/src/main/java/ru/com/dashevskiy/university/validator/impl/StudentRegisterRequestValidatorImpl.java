package ru.com.dashevskiy.university.validator.impl;

import org.apache.commons.validator.routines.EmailValidator;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.validator.StudentRegisterRequestValidator;

@Component
@RequiredArgsConstructor
@Slf4j
public class StudentRegisterRequestValidatorImpl implements StudentRegisterRequestValidator {
    private final PasswordValidator passwordValidator;
    private final EmailValidator emailValidator;
    private final StudentCrud studentCrud;

    @Override
    public void validate(StudentRegisterRequest registerRequest) {        
        log.debug("Validating student register request - {}", registerRequest);
        log.trace("Not empty and not null checks");
        validateNotNull(registerRequest.getEmail(), "Email is null");        
        validateNotNull(registerRequest.getPasswordFirstTry(), "First try password is null");
        validateNotNull(registerRequest.getPasswordSecondTry(), "Second try password is null");
        
        validateEmailIsUnique(registerRequest.getEmail());
        validateEmailFormat(registerRequest.getEmail());
        
        validatePasswordsAreEqual(registerRequest.getPasswordFirstTry(), registerRequest.getPasswordSecondTry());
        validatePasswordFormat(registerRequest.getPasswordFirstTry());                
    }
    
    void validatePasswordsAreEqual(String passwordFirstTry, String passwordSecondTry) {
        log.trace("Checking passwords are equal");
        if (!passwordFirstTry.contentEquals(passwordSecondTry)) {
            throw new IllegalArgumentException("Passwords are not equal.");
        }
        
    }
    
    void validatePasswordFormat(String password) {
        log.trace("Checking passwords has valid format");
        RuleResult passwordValidatonResult = passwordValidator.validate(new PasswordData(password));
        if (!passwordValidatonResult.isValid()) {
            throw new IllegalArgumentException("Wrong password error.");
        }       
    }
    
    void validateEmailIsUnique(String email) {        
        log.trace("Checking email - {} is unique", email);
        if (studentCrud.findStudentByEmail(email).isPresent()) {
            throw new IllegalArgumentException("Email is not unique.");
        }
    }
    
    void validateEmailFormat(String email) {        
        log.trace("Checking email - {} has valid format", email);
        if (!emailValidator.isValid(email)) {
            throw new IllegalArgumentException("Wrong email format.");
        }        
    }   
}
