package ru.com.dashevskiy.university.validator.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;
import ru.com.dashevskiy.university.entity.user.Student;
import ru.com.dashevskiy.university.validator.GroupStudenAssignationRequestValidator;

@Component
@RequiredArgsConstructor
@Slf4j
public class GroupStudenAssignationRequestValidatorImpl implements GroupStudenAssignationRequestValidator {
    private final static String ID_DELIMITER = ", ";
    private final static String CONFLICTING_EXEPTION_MESSAGE = 
            "Students with ids %s have already been assigned to group with - %d";
    private final static String MISSING_EXEPTIION_MESSAGE = 
            "Students with ids %s are not existing.";
    private final static String NO_SUCH_GROUP = 
            "Group with id %d is not existing.";
    
    private final StudentCrud studentCrud;
    private final GroupCrud groupCrud; 

    @Override
    public void validate(GroupStudenAssignationRequest groupStudenAssignationRequest) {
        log.debug("Validating group studen assignation request - {}", groupStudenAssignationRequest);
        validateGroupExists(groupStudenAssignationRequest.getGroupId());
        validateStudentsExist(groupStudenAssignationRequest.getStudentIds());
        validateNoSuchAssignationExistAlready(groupStudenAssignationRequest);
        
    }

    void validateGroupExists(Integer groupId) {
        log.trace("Checking group with id - {} exists", groupId);
        groupCrud.findById(groupId).orElseThrow(
                () -> new NoSuchElementException(String.format(NO_SUCH_GROUP, groupId)));        
    }

    void validateStudentsExist(Set<Integer> studentIds) {
        log.trace("Checking students with id - {} exist", studentIds);
        List<Integer> studentNotExistingInDBids = getMissingStudentsIds(studentIds);
        
        if (!studentNotExistingInDBids.isEmpty()) {
            throw new NoSuchElementException(
                    String.format(MISSING_EXEPTIION_MESSAGE, getIdListing(studentNotExistingInDBids)));
        }
        
    }   

    void validateNoSuchAssignationExistAlready(GroupStudenAssignationRequest groupStudenAssignationRequest) {                
        log.trace("Checking no such assignations assigned already for - {}", groupStudenAssignationRequest);
        List<Integer> conflictingAssignationStudentIds = getConflictingStudentsId(groupStudenAssignationRequest);        
        
        if (!conflictingAssignationStudentIds.isEmpty()) {            
            throw new IllegalArgumentException(getConflictingStudentsExeptionMessage(groupStudenAssignationRequest, 
                                                                                     conflictingAssignationStudentIds));
        }        
    }
    
    private List<Integer> getMissingStudentsIds(Set<Integer> studentIds) {
        return studentIds.stream()
        .collect(Collectors.toMap(id -> id, studentCrud::findById)).entrySet().stream()
        .filter(entry -> !entry.getValue().isPresent())
        .map(entry -> entry.getKey())
        .collect(Collectors.toList());
    }

    private List<Integer> getConflictingStudentsId(GroupStudenAssignationRequest groupStudenAssignationRequest) {
        Set<Integer> studentForAssigmentIds = groupStudenAssignationRequest.getStudentIds();        
        
        return studentCrud.findStudentsByGroupId(groupStudenAssignationRequest.getGroupId()).stream()
               .map(Student::getId)
               .filter(studentForAssigmentIds::contains)
               .collect(Collectors.toList());
    }

    private String getConflictingStudentsExeptionMessage(GroupStudenAssignationRequest groupStudenAssignationRequest,
                                      List<Integer> conflictingAssignationStudentIds) {
        return String.format(CONFLICTING_EXEPTION_MESSAGE, getIdListing(conflictingAssignationStudentIds),
                                                groupStudenAssignationRequest.getStudentIds());
    }

    private String getIdListing(List<Integer> studentIds) {
        return studentIds.stream()
               .map((id) -> id.toString())
               .collect(Collectors.joining(ID_DELIMITER));
    }
}
