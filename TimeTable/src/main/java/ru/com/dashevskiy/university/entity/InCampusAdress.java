package ru.com.dashevskiy.university.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder(setterPrefix = "with", builderClassName = "Builder")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class InCampusAdress {
    
    @EqualsAndHashCode.Exclude 
    Integer id;
    String building;
    int floor;
    int entry;
}
