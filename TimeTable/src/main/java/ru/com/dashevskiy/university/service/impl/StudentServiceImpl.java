package ru.com.dashevskiy.university.service.impl;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;
import ru.com.dashevskiy.university.domain.student.StudentResponse;
import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;
import ru.com.dashevskiy.university.entity.user.Student;
import ru.com.dashevskiy.university.service.StudentService;
import ru.com.dashevskiy.university.service.mapper.StudentMapper;
import ru.com.dashevskiy.university.service.mapper.StudentRegisterRequestMapper;
import ru.com.dashevskiy.university.service.mapper.StudentUpdatePersonalDataRequestMapper;
import ru.com.dashevskiy.university.validator.Validator;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {
    private static final String DID_NOT_FIND_BY_ID_MESSAGE = "Did not find student with such id - %d";
    private static final String DID_NOT_FIND_BY_EMAIL_MESSAGE = "Did not find student with such email - %s";

    private final PasswordEncoder passwordEncoder;
    private final StudentCrud studentCrud;    
    private final Validator<StudentUpdatePersonalDataRequest> updatePersonalDataRequestValidator; 
    private final Validator<StudentRegisterRequest> registerRequestValidator;       
    private final StudentRegisterRequestMapper registerRequestMapper;
    private final StudentUpdatePersonalDataRequestMapper updatePersonalDataRequestMapper;
    private final StudentMapper studentMapper;    

    @Override
    public boolean login(String email, String password) {
        log.debug("Login started with email - {}", email);
        Student student = studentCrud.findStudentByEmail(email)
                .orElseThrow(() -> new NoSuchElementException(String.format(DID_NOT_FIND_BY_EMAIL_MESSAGE, email)));
        
        log.trace("Matching given password with stored password with password hash");
        return passwordEncoder.matches(password, student.getPassword());
    }

    @Override
    public void register(StudentRegisterRequest registerRequest) {
        log.debug("Register started with registerRequest - {}", registerRequest);
        registerRequestValidator.validate(registerRequest);
        studentCrud.save(getStudent(registerRequest));
    }

    @Override
    public StudentResponse find(int studentId) {
        log.debug("Retriving student response responce by Id - {} started", studentId);
        Student student = studentCrud.findById(studentId)
                .orElseThrow(() -> new NoSuchElementException(String.format(DID_NOT_FIND_BY_ID_MESSAGE, studentId)));
        
        return studentMapper.mapEntityToDomain(student);
    }

    @Override
    public List<StudentResponse> findAll(int page, int itemPerPage) {
        log.debug("Retriving all student responses on page - {}, with items per page - {}", page, itemPerPage);
        return studentCrud.findAll(page, itemPerPage).stream()
               .map(studentMapper::mapEntityToDomain)
               .collect(Collectors.toList());
    }

    @Override
    public List<StudentResponse> findStudentsByGroup(int groupId) {
        log.debug("Retriving student responses by group id - {}", groupId);
        return studentCrud.findStudentsByGroupId(groupId).stream()
                .map(studentMapper::mapEntityToDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void updatePersonalData(StudentUpdatePersonalDataRequest updateRequest) {
        log.debug("Updating student with student update personal data request - {}", updateRequest);
        updatePersonalDataRequestValidator.validate(updateRequest);
        studentCrud.update(getStudentForPersonalDataUpdate(updateRequest));
    }

    @Override
    public int count() {
        log.debug("Counting students");
        return studentCrud.count();
    }

    private Student getStudentForPersonalDataUpdate(StudentUpdatePersonalDataRequest updateRequest) {
        return updatePersonalDataRequestMapper.mapDomainToEntity(updateRequest, getStudentForUpdate(updateRequest));
    }

    private Student getStudentForUpdate(StudentUpdatePersonalDataRequest updateRequest) {
        return studentCrud.findById(updateRequest.getId()).get();
    }

    private Student getStudent(StudentRegisterRequest registerRequest) {
        return registerRequestMapper.mapDomainToEntity(registerRequest, getEncodedPassword(registerRequest));
    }

    private String getEncodedPassword(StudentRegisterRequest registerRequest) {
        return passwordEncoder.encode(registerRequest.getPasswordFirstTry());
    }
}
