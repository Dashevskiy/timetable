package ru.com.dashevskiy.university.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.entity.user.Student;

@Slf4j
@Repository
public class StudentCrudImpl extends AbstractCrudDaoImpl<Student> implements StudentCrud {
    private static final RowMapper<Student> ROW_MAPPER = (resultSet, rowNumber) -> 
            Student.builder()
                .withId(resultSet.getInt("id"))
                .withInactive(resultSet.getBoolean("inactive"))
                .withEmail(resultSet.getString("email"))
                .withPassword(resultSet.getString("password"))
                .withFirstname(resultSet.getString("firstname"))
                .withSecondname(resultSet.getString("secondname"))
                .withStudentRole(Student.Role.valueOf(resultSet.getString("student_role")))
                .build();
    
    private static final String SAVE_QUERY = 
            "INSERT INTO users (status_id, inactive, email, password, firstname, secondname, student_role, class_mark)" + 
            "VALUES (1, :inactive, :email, :password, :firstname, :secondname, :studentRole, 'student')";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM users WHERE id = :id AND class_mark = 'student'";
    private static final String FIND_BY_EMAIL_QUERY = 
            "SELECT * FROM users WHERE email = :email AND class_mark = 'student' ORDER BY id";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM users WHERE class_mark = 'student' ORDER BY id";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM users WHERE class_mark = 'student' ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String FIND_STUDENTS_BY_GROUP_ID = 
            "SELECT " + 
            "    * " + 
            "FROM " + 
            "    users " + 
            "INNER JOIN " + 
            "    groups_to_sutdents_assigments " + 
            "        ON users.id = groups_to_sutdents_assigments.student_id " + 
            "        AND groups_to_sutdents_assigments.group_id = :groupId";
    private static final String UPDATE_QUERY = 
            "UPDATE users " + 
            "SET " +
            "    inactive = :inactive, " + 
            "    email = :email, " + 
            "    password = :password, " +
            "    firstname = :firstname, " + 
            "    secondname = :secondname, " + 
            "    student_role = :studentRole, " + 
            "    class_mark = 'student' " +
            "WHERE " + 
            "    id = :id";    
    private static final String DELETE_QUERY = 
            "DELETE FROM users WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM users WHERE class_mark = 'student' ";

    public StudentCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id",
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
    }

    @Override
    public List<Student> findStudentsByGroupId(Integer groupId) {
        log.debug("Finding students by group id - {}", groupId);
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("groupId", groupId);
        return jdbcTemplate.query(FIND_STUDENTS_BY_GROUP_ID, parameters, ROW_MAPPER);
    }

    @Override
    public Optional<Student> findStudentByEmail(String email) {
        log.debug("Finding students by email- {}", email);
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("email", email);
        try {
            
            return Optional.of(jdbcTemplate.queryForObject(FIND_BY_EMAIL_QUERY, parameters, ROW_MAPPER));
        } catch (EmptyResultDataAccessException exeption) {
            return Optional.empty();
        }
    }
}
