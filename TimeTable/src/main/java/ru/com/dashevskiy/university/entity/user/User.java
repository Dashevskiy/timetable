package ru.com.dashevskiy.university.entity.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
public abstract class User {
    
    @EqualsAndHashCode.Exclude
    protected  Integer id;
    
    @EqualsAndHashCode.Exclude
    protected  boolean inactive;
    
    protected  String email;
    
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    protected  String password;
    protected  String firstname;
    protected  String secondname;
}
