package ru.com.dashevskiy.university.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.CrudDao;
import ru.com.dashevskiy.university.dao.parametersource.BeanPorpertySqlParameterSourceWithEnumToString;

@AllArgsConstructor
@Slf4j
public abstract class AbstractCrudDaoImpl<D> implements CrudDao<D> {      
    protected final NamedParameterJdbcTemplate jdbcTemplate;    
    private final RowMapper<D> rowMapper;
    private final String idColumnName;
    private final String saveQuery;
    private final String findByIdQuery;
    private final String findAllQuery;
    private final String findAllQueryWithPagination;
    private final String updateQuery;
    private final String deleteQuery;
    private final String countQuery;
    
    @Override
    public Integer save(D entity) {
        log.debug("Saving entuty - {}", entity);
        SqlParameterSource parameters = new BeanPorpertySqlParameterSourceWithEnumToString(entity);
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(saveQuery, parameters, keyHolder);
        
        return (Integer) keyHolder.getKey();
    }

    @Override
    public Optional<D> findById(int id) {
        log.debug("Finding entuty by id - {}", id);
        SqlParameterSource parameters = new MapSqlParameterSource(idColumnName, id);
        try {
            return Optional.of(jdbcTemplate.queryForObject(findByIdQuery, parameters, rowMapper));
        } catch (EmptyResultDataAccessException  exeption) {
            return Optional.empty();
        }
    }
    
    @Override
    public List<D> findAll() {
        log.debug("Finding all entities");
        return jdbcTemplate.query(findAllQuery, rowMapper);
    }

    @Override
    public List<D> findAll(int page, int itemPerPage) {
        log.debug("Finding all entities within page - {} with items per page - {}", page, itemPerPage);
        SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("limit", itemPerPage)
                    .addValue("offset", itemPerPage * page);
        
        return jdbcTemplate.query(findAllQueryWithPagination, parameters, rowMapper);
    }

    @Override
    public void update(D entity) {
        log.debug("Updating entity - {}", entity);
        SqlParameterSource parameters = new BeanPorpertySqlParameterSourceWithEnumToString(entity);
        jdbcTemplate.update(updateQuery, parameters);
    }

    @Override
    public void deleteById(int id) {
        log.debug("Delting entity by Id - {}", id);
        SqlParameterSource parameters = new MapSqlParameterSource(idColumnName, id);
        jdbcTemplate.update(deleteQuery, parameters);
    }

    @Override
    public int count() {
        log.debug("Counting entity");
        SqlParameterSource parameters = new MapSqlParameterSource();
        return jdbcTemplate.queryForObject(countQuery, parameters, Integer.class);
    }
}
