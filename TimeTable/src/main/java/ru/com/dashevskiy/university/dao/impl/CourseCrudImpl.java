package ru.com.dashevskiy.university.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.CourseCrud;
import ru.com.dashevskiy.university.entity.Course;

@Slf4j
@Repository
public class CourseCrudImpl extends AbstractCrudDaoImpl<Course> implements CourseCrud {
    private static final RowMapper<Course> ROW_MAPPER = (resultSet, rowNumber) -> 
            new Course(resultSet.getInt("id"), 
                       resultSet.getString("description"),
                       resultSet.getString("name"));
    
    private static final String SAVE_QUERY =
            "INSERT INTO courses (description, name) " + 
            "VALUES (:description, :name)";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM courses WHERE id = :id";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM courses";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM courses ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String UPDATE_QUERY = 
            "UPDATE courses " + 
            "SET " + 
            "    description = :description, " + 
            "    name = :name " + 
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM courses WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM courses";

    public CourseCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id",
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
     }
}
