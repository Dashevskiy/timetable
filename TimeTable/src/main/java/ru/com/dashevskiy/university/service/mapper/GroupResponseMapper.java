package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.Mapper;

import ru.com.dashevskiy.university.domain.group.GroupResponse;
import ru.com.dashevskiy.university.entity.Group;

@Mapper(componentModel = "spring")
public interface GroupResponseMapper {

    GroupResponse mapDomainToEntity(Group addRequest);
    
}
