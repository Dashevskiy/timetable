package ru.com.dashevskiy.university.entity.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Student extends User {    
    
    @EqualsAndHashCode.Exclude 
    private Role studentRole;
    
    @Builder(builderClassName = "StudentBuilder", setterPrefix = "with", toBuilder = true)
    public Student(Integer id,
                   boolean inactive,
                   String email,
                   String password,
                   String firstname, 
                   String secondname, 
                   Role studentRole) {        
        super(id,
              inactive,
              email,
              password,
              firstname, 
              secondname);
        this.studentRole = studentRole;
    }
    
    public static class StudentBuilder{
        private Role studentRole = Role.REGULAR;
    }
    
    public enum Role{
        REGULAR,
        HEADMAN,
        LIBRARIAN,
        ENTERTAINER
    }    
}
