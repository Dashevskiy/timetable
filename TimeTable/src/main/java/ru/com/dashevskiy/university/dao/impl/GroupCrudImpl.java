package ru.com.dashevskiy.university.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.entity.Group;

@Slf4j
@Repository
public class GroupCrudImpl extends AbstractCrudDaoImpl<Group> implements GroupCrud {
    private static final RowMapper<Group> ROW_MAPPER = (resultSet, rowNumber) -> 
            new Group(resultSet.getInt("id"),
                      resultSet.getBoolean("inactive"),
                      resultSet.getString("name"));

    private static final String SAVE_QUERY =
            "INSERT INTO groups(name, inactive) " +
            "VALUES (:name, :inactive)";
    private static final String ASSIGN_STUDENT_TO_GROUP_QUERY = 
            "INSERT INTO groups_to_sutdents_assigments(student_id, group_id) " + 
            "VALUES (:studentId, :groupId)";
    private static final String UPDATE_STUDENT_STATUS = 
            "UPDATE users SET status_id = 2 WHERE id = :studentId";
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM groups WHERE id = :id";
    private static final String FIND_BY_NAME_QUERY = 
            "SELECT * FROM groups WHERE name = :name";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM groups";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM groups ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String FIND_GROUPS_BY_LESSIN_ID_QUERY = 
            "SELECT " + 
            "    * " + 
            "FROM " + 
            "    groups " + 
            "INNER JOIN " + 
            "    lessons_to_groups_assigments " + 
            "        ON groups.id = lessons_to_groups_assigments.group_id " + 
            "        AND lessons_to_groups_assigments.lesson_id = :lessonId";
    private static final String UPDATE_QUERY = 
            "UPDATE groups " + 
            "SET " + 
            "    inactive = :inactive, " + 
            "    name = :name " + 
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM groups WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM groups";
    private static final String REMOVE_STUDENT_FROM_GROUP_QUERY = 
            "DELETE FROM groups_to_sutdents_assigments WHERE student_id = :studentId AND group_id = :groupId";

    public GroupCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id",
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
     }

    @Override
    public void assignStudentsToGroup(Integer groupId, List<Integer> studentIds) {
        log.debug("Assigning to group - {} students - {}", groupId, studentIds);
        final SqlParameterSource[] groupStudentsBathArguments = getGroupStudentsBathArguments(groupId, studentIds);
        jdbcTemplate.batchUpdate(ASSIGN_STUDENT_TO_GROUP_QUERY, groupStudentsBathArguments);
        jdbcTemplate.batchUpdate(UPDATE_STUDENT_STATUS, groupStudentsBathArguments);
    }

    @Override
    public void removeStudentsFromGroup(Integer groupId, List<Integer> studentIds) {
        log.debug("Removing students from group - {} students - {}", groupId, studentIds);
        jdbcTemplate.batchUpdate(REMOVE_STUDENT_FROM_GROUP_QUERY, 
                                 getGroupStudentsBathArguments(groupId, studentIds));
    }

    @Override
    public List<Group> findByLessonId(Integer lessonId) {
        log.debug("Finding groups by lesson id - {}", lessonId);
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("lessonId", lessonId);
        return jdbcTemplate.query(FIND_GROUPS_BY_LESSIN_ID_QUERY, parameters, ROW_MAPPER);
    }

    @Override
    public Optional<Group> findByName(String name) {
        log.debug("Finding groups by name - {}", name);
        SqlParameterSource parameters = new MapSqlParameterSource("name", name);
        try {
            return Optional.of(jdbcTemplate.queryForObject(FIND_BY_NAME_QUERY, parameters, ROW_MAPPER));
        } catch (EmptyResultDataAccessException  exeption) {
            return Optional.empty();
        }
    }

    private SqlParameterSource[] getGroupStudentsBathArguments(Integer groupId, List<Integer> studentIds) {
        return studentIds.stream()
               .map(studentId -> new MapSqlParameterSource()
                                     .addValue("groupId", groupId)
                                     .addValue("studentId", studentId))
               .toArray(SqlParameterSource[]::new);        
    }
}
