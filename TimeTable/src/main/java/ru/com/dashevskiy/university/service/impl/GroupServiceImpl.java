package ru.com.dashevskiy.university.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.dao.StudentCrud;
import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.domain.group.GroupResponse;
import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;
import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.domain.student.StudentResponse;
import ru.com.dashevskiy.university.entity.Group;
import ru.com.dashevskiy.university.service.GroupService;
import ru.com.dashevskiy.university.service.mapper.GroupAddRequestMapper;
import ru.com.dashevskiy.university.service.mapper.GroupResponseMapper;
import ru.com.dashevskiy.university.service.mapper.GroupUpdateRequestMapper;
import ru.com.dashevskiy.university.service.mapper.StudentMapper;
import ru.com.dashevskiy.university.validator.Validator;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class GroupServiceImpl implements GroupService {
    private static final String DID_NOT_FIND_BY_ID = "Did not find group with such id - %d";
    
    private final StudentCrud studentCrud;
    private final GroupCrud groupCrud;
    private final StudentMapper studentMapper;
    private final GroupAddRequestMapper groupAddRequestMapper;
    private final GroupUpdateRequestMapper groupUpdateRequestMapper;
    private final GroupResponseMapper groupResponseMapper;
    private final Validator<GroupAddRequest> groupAddRequestVallidator;
    private final Validator<GroupUpdateRequest> groupUpdateRequestVallidator;
    private final Validator<GroupStudenAssignationRequest> groupStudenAssignationRequestValidator;    

    @Override
    public void add(GroupAddRequest addRequest) {
        log.debug("Adding with group request - {}", addRequest);
        groupAddRequestVallidator.validate(addRequest);
        groupCrud.save(groupAddRequestMapper.mapEntityToDomain(addRequest));      
    }

    @Override
    public GroupResponse find(Integer groupId) {
        log.debug("Retriving group responce by group id - {}", groupId);
        Group group = groupCrud.findById(groupId)
                .orElseThrow(() -> new NoSuchElementException(String.format(DID_NOT_FIND_BY_ID, groupId)));
        
        return groupResponseMapper.mapDomainToEntity(group);
    }

    @Override
    public List<GroupResponse> findAll(int page, int itemPerPage) {
        log.debug("Retriving all group responses on page - {}, with items per page - {}", page, itemPerPage);
        return groupCrud.findAll(page, itemPerPage).stream()
               .map(groupResponseMapper::mapDomainToEntity)
               .collect(Collectors.toList());        
    }

    @Override
    public void update(GroupUpdateRequest groupUpdateRequest) {
        log.debug("Doing group update request - {}", groupUpdateRequest);
        groupUpdateRequestVallidator.validate(groupUpdateRequest);
        groupCrud.update(groupUpdateRequestMapper.mapEntityToDomain(groupUpdateRequest));        
    }

    @Override
    public void assighStudentsToGroup(GroupStudenAssignationRequest assignationRequest) {
        log.debug("Assigning to group - {} students - {}", 
                  assignationRequest.getGroupId(), 
                  assignationRequest.getStudentIds());
        groupStudenAssignationRequestValidator.validate(assignationRequest);
        groupCrud.assignStudentsToGroup(assignationRequest.getGroupId(), 
                                        new ArrayList<>(assignationRequest.getStudentIds()));        
    }

    @Override
    public void removeStudentFromGroup(Set<Integer> studentIds, Integer groupId) {
        log.debug("Removing from group - {} students - {}", groupId, studentIds);
        groupCrud.removeStudentsFromGroup(groupId, new ArrayList<>(studentIds));        
    }

    @Override
    public List<StudentResponse> getStudents(Integer groupId) {               
        log.debug("Retriving student responses by group id - {}", groupId);                  
        return studentCrud.findStudentsByGroupId(groupId).stream()
               .map(studentMapper::mapEntityToDomain)
               .collect(Collectors.toList());
    }
    
    @Override
    public int count() {
        log.debug("Counting groups");
        return groupCrud.count();
    }
}
