package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.Mapper;

import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;
import ru.com.dashevskiy.university.entity.user.Student;

@Mapper(componentModel = "spring")
public interface StudentUpdatePersonalDataRequestMapper {
    
    default Student mapDomainToEntity(StudentUpdatePersonalDataRequest domain, Student studentForUpdate) {        
        return studentForUpdate.toBuilder()
               .withFirstname(domain.getFirstname())
               .withSecondname(domain.getSecondname())
               .build();
    }    
}
