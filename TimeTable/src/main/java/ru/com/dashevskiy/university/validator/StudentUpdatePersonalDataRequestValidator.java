package ru.com.dashevskiy.university.validator;

import ru.com.dashevskiy.university.domain.student.StudentUpdatePersonalDataRequest;

public interface StudentUpdatePersonalDataRequestValidator extends Validator<StudentUpdatePersonalDataRequest> {
}
