package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.Mapper;

import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.entity.Group;

@Mapper(componentModel = "spring")
public interface GroupUpdateRequestMapper {

    Group mapEntityToDomain(GroupUpdateRequest updateRequest);
    
}
