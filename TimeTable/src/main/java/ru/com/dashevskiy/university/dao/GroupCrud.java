package ru.com.dashevskiy.university.dao;

import java.util.List;
import java.util.Optional;

import ru.com.dashevskiy.university.entity.Group;

public interface GroupCrud extends CrudDao<Group> {
    
    void assignStudentsToGroup(Integer groupId, List<Integer> studentIds);
    
    void removeStudentsFromGroup(Integer groupId, List<Integer> studentIds);
    
    List<Group> findByLessonId(Integer lessonId);
    
    Optional<Group> findByName(String name);
    
}
