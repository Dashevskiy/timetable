package ru.com.dashevskiy.university.validator;

import ru.com.dashevskiy.university.domain.student.StudentRegisterRequest;

public interface StudentRegisterRequestValidator extends Validator<StudentRegisterRequest> {    
}
