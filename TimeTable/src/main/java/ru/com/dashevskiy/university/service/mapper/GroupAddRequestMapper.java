package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.Mapper;

import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.entity.Group;

@Mapper(componentModel = "spring")
public interface GroupAddRequestMapper {

    Group mapEntityToDomain(GroupAddRequest addRequest);
    
}
