package ru.com.dashevskiy.university.validator;

import java.util.Objects;

public interface Validator<D> {
    
    void validate(D object);
    
    default void validateNotNull(Object value, String exeptionMessage) {        
        if (Objects.isNull(value)) {
            throw new IllegalArgumentException(exeptionMessage);            
        }        
    }
    
    default void validateNotEmpty(String value, String exeptionMessage) {
        if (value.isEmpty()) {
            throw new IllegalArgumentException(exeptionMessage);
        }
    }
}
