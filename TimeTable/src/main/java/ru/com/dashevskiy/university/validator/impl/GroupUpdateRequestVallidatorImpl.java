package ru.com.dashevskiy.university.validator.impl;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.GroupCrud;
import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.validator.GroupUpdateRequestVallidator;

@Component
@RequiredArgsConstructor
@Slf4j
public class GroupUpdateRequestVallidatorImpl implements GroupUpdateRequestVallidator {
    private final GroupCrud groupCrud;    

    @Override
    public void validate(GroupUpdateRequest groupUpdateRequest) {
        log.debug("Validating group update request - {}", groupUpdateRequest);
        log.trace("Not empty and not null checks");
        validateNotNull(groupUpdateRequest.getName(), "Name is null.");
        validateNotEmpty(groupUpdateRequest.getName(), "Name is empty.");
        
        validateNameIsUnique(groupUpdateRequest.getName());
    }

    void validateNameIsUnique(String name) {
        log.trace("Checking group name - {} is unique", name);
        groupCrud.findByName(name).orElseThrow(
                () -> new IllegalArgumentException("Name is not unique."));        
    }
}
