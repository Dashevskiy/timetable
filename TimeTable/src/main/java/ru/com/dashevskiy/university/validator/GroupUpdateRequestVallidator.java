package ru.com.dashevskiy.university.validator;

import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;

public interface GroupUpdateRequestVallidator extends Validator<GroupUpdateRequest> {
}
