package ru.com.dashevskiy.university.dao;

import java.util.List;
import java.util.Optional;

public interface CrudDao<D> {
    
    Integer save(D domainObject);
    
    Optional<D> findById(int id);
    
    List<D> findAll();
    
    List<D> findAll(int page, int itemPerPage);
    
    void update(D domainObject);
    
    void deleteById(int id);
    
    int count();
    
}
