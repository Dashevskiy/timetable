package ru.com.dashevskiy.university.dao;

import ru.com.dashevskiy.university.entity.Course;

public interface CourseCrud extends CrudDao<Course> {    
}
