package ru.com.dashevskiy.university.entity;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
public class Course {
    
    @EqualsAndHashCode.Exclude 
    Integer id;
    
    @EqualsAndHashCode.Exclude 
    String description;    
    String name;    
}
