package ru.com.dashevskiy.university.dao.impl;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import ru.com.dashevskiy.university.dao.InCampusAdressCrud;
import ru.com.dashevskiy.university.entity.InCampusAdress;

@Slf4j
@Repository
public class InCampusAdressCrudImpl extends AbstractCrudDaoImpl<InCampusAdress> implements InCampusAdressCrud {
    private static final RowMapper<InCampusAdress> ROW_MAPPER = (resultSet, rowNumber) -> 
        InCampusAdress.builder()
            .withId(resultSet.getInt("id"))
            .withBuilding(resultSet.getString("building"))
            .withEntry(resultSet.getInt("entry"))
            .withFloor(resultSet.getInt("floor"))
            .build();
    private static final String SAVE_QUERY = 
            "INSERT INTO in_campus_addresses (building, entry, floor) " + 
            "VALUES (:building, :entry, :floor) "; 
    private static final String FIND_BY_ID_QUERY = 
            "SELECT * FROM in_campus_addresses WHERE id = :id";
    private static final String FIND_ALL_QUERY = 
            "SELECT * FROM in_campus_addresses";
    private static final String FIND_ALL_WITH_PAGINATION_QUERY = 
            "SELECT * FROM in_campus_addresses ORDER BY id LIMIT :limit OFFSET :offset";
    private static final String UPDATE_QUERY = 
            "UPDATE in_campus_addresses " + 
            "SET " + 
            "    building = :building, " + 
            "    entry = :entry, " + 
            "    floor = :floor " + 
            "WHERE " + 
            "    id = :id";
    private static final String DELETE_QUERY = 
            "DELETE FROM in_campus_addresses WHERE id =:id";
    private static final String COUNT_QUERY = 
            "SELECT COUNT(id) FROM in_campus_addresses";

    public InCampusAdressCrudImpl(NamedParameterJdbcTemplate jdbcTemplate){
        super(jdbcTemplate,
              ROW_MAPPER, 
              "id",
              SAVE_QUERY,
              FIND_BY_ID_QUERY, 
              FIND_ALL_QUERY, 
              FIND_ALL_WITH_PAGINATION_QUERY,
              UPDATE_QUERY, 
              DELETE_QUERY,
              COUNT_QUERY);
     }
}
