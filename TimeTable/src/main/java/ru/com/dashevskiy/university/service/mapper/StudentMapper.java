package ru.com.dashevskiy.university.service.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import ru.com.dashevskiy.university.domain.student.StudentResponse;
import ru.com.dashevskiy.university.entity.user.Student;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface StudentMapper {    

    StudentResponse mapEntityToDomain(Student student);
    
}
