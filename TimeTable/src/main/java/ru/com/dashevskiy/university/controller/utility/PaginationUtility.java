package ru.com.dashevskiy.university.controller.utility;

import org.apache.commons.lang3.math.NumberUtils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PaginationUtility {

    public Integer getPageNumberOrDefaultIfNotValid(String pageParameter, 
                                                    Integer pagesTotal,
                                                    Integer defaultPage) {
        Integer page = NumberUtils.toInt(pageParameter, defaultPage);
        
        if (page < 1) {
            return defaultPage;
        }
        
        if (page > getMaximalPage(pagesTotal)) {
            return getMaximalPage(pagesTotal);
        }
        
        return page;
    }
    
    public Integer getTotalPages(int entityCount, int itemPerPage) {
        return entityCount/itemPerPage;
    }
    
    private int getMaximalPage(Integer pagesTotal) {
        return pagesTotal + 1;
    }
}
