package ru.com.dashevskiy.university.service;

import java.util.List;
import java.util.Set;

import ru.com.dashevskiy.university.domain.group.GroupAddRequest;
import ru.com.dashevskiy.university.domain.group.GroupResponse;
import ru.com.dashevskiy.university.domain.group.GroupStudenAssignationRequest;
import ru.com.dashevskiy.university.domain.group.GroupUpdateRequest;
import ru.com.dashevskiy.university.domain.student.StudentResponse;

public interface GroupService {
    
    void add(GroupAddRequest addReq);
    
    GroupResponse find(Integer groupId);
    
    List<GroupResponse> findAll(int page, int itemPerPage);
    
    void update(GroupUpdateRequest groupUpdateRequest); 
    
    void assighStudentsToGroup(GroupStudenAssignationRequest assignationRequest);
    
    void removeStudentFromGroup(Set<Integer> studentIds, Integer groupId);
    
    List<StudentResponse> getStudents(Integer groupId);
    
    int count();
    
}
