package ru.com.dashevskiy.university.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Group {
    
    @EqualsAndHashCode.Exclude 
    private Integer id; 
    
    @EqualsAndHashCode.Exclude
    private boolean inactive;    
    private String name;    
}
