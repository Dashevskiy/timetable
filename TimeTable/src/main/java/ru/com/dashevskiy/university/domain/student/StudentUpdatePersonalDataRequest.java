package ru.com.dashevskiy.university.domain.student;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class StudentUpdatePersonalDataRequest {    
    private  Integer id;    
    private  String firstname;
    private  String secondname;
}
